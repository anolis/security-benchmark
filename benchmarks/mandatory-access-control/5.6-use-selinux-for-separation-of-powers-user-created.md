# 5.6 使用SELinux实现三权分离-用户创建

## 安全等级
- Level 4

## 描述

> * 当前，Linux操作系统已广泛应用于各种设备和产品中，如服务器、PC机、机顶盒及路由器等。随着Linux系统的不断发展和广泛应用，Linux系统的安全问题也引起越来越多的关注。
> * 在Linux操作系统中，存在一个超级用户即root用户。root也称为系统管理员，它拥有管理系统的一切权限。当一个非法用户获得root用户口令后，他就可以以超级用户的身份登录系统，然后做任何他想做的事情：如任意添加、删除用户，终止进程，删除重要文件甚至更改root用户的口令。因此，一旦root权限被恶意用户利用，就可能导致系统数据遭到泄密和破坏。
> * 该问题也引起了国家的重点关注，如国家保密标准BMB20-2007《涉及国家秘密的信息系统分级保护管理规范》中明确提出：涉密信息系统应配备系统管理员、安全保密管理员和安全审计员这三类安全保密管理人员，三员应该相互独立、相互制约、不得兼任。三个管理员之间的工作机制分为协作和制约两种机制，行使的是原超级用户的权力，即系统管理员、安全管理员和审计管理员间相互协作，共同维护系统的正常运行。制约机制指只有在当前管理员操作不影响其他管理员正在进行的操作时才被允许，从而保证了管理员行为的可预期性，避免超级用户的误操作或其身份被假冒而带来的安全隐患，增强了系统的安全性。该规范可以有效防止由系统管理员权力过大所带来的系统安全威胁和隐患。

> * SELinux (security-enhanced Linux)是安全增强的Linux，以强制访问控制(mandatory access control, MAC)技术为基础，应用类型增强(type enforcement, TE)和基于角色访问控制(role-base access control, RBAC)两种安全策略模型。通过MAC技术可以实现对用户和进程权限的最小化，即使在系统受到攻击或者进程和用户的权限被剥夺的情况下，也不会对整个系统的安全造成重大影响。SELinux对访问的控制更彻底，它对系统中的所有文件、目录、端口资源的访问控制都基于一定的安全策略而设定。只有管理员才能定制安全策略，一般用户没有权限更改。因此SELinux为三权分离思想的实现奠定了基础。

## 前置条件

SElinux服务正确安装，且服务正常开启。具体可参考以下文档：

* 5.1-ensure-selinux-is-installed.md  ->  安装SElinux工具
* 5.3-ensure-the-selinux-mode-is-enabled.md  ->  启用SElinux服务

## 修复建议

* 创建系统、安全、审计管理员用户

1. 创建 SElinux 系统管理员用户

```bash
# useradd -m -s /bin/bash sysadm
# passwd sysadm
# echo 'export PATH="/sbin:/bin:/usr/sbin:/usr/bin:/usr/X11R6/bin:/sbin:/usr/sbin"' >> /home/sysadm/.bashrc
# usermod sysadm -aG wheel
# semanage user -m -R 'sysadm_r system_r' sysadm_u
# semanage login -a -s sysadm_u sysadm
# restorecon -FR -v /home/sysadm
```

2. 创建 SElinux 安全管理员用户

```bash
# useradd -m -s /bin/bash secadm
# passwd secadm
# echo 'export PATH="/sbin:/bin:/usr/sbin:/usr/bin:/usr/X11R6/bin:/sbin:/usr/sbin"' >> /home/secadm/.bashrc
# usermod secadm -aG wheel
# semanage user -a -R "staff_r secadm_r" -P staff secadm_u
# semanage login -a -s secadm_u secadm
# restorecon -FR -v /home/secadm
```

3. 创建 SElinux 审计管理员用户

```bash
# useradd -m -s /bin/bash audadm
# passwd audadm
# echo 'export PATH="/sbin:/bin:/usr/sbin:/usr/bin:/usr/X11R6/bin:/sbin:/usr/sbin"' >> /home/audadm/.bashrc
# usermod audadm -aG wheel
# semanage user -a -R "staff_r auditadm_r" -P staff auditadm_u
# semanage login -a -s auditadm_u audadm
# restorecon -FR -v /home/audadm
```

* 应用`mls`策略
1. 安装`selinux-policy-mls`策略软件包：

```bash
# yum install -y selinux-policy-mls
```

2. 修改`/etc/selinux/config`文件中`SELINUXTYPE`变量的值，将安全策略配置为`mls`：

```bash
SELINUXTYPE=mls
```
* 重启系统
1. 重启系统，使配置生效：

```bash
# reboot
```

## 扫描检测

1. 查看selinux状态

```bash
# getenforce
Permissive
OR
Enforcing
```
2. 查看selinux策略

```bash
# sestatus | grep Loaded
Loaded policy name:             mls
```

3. 查看创建三个用户状态

```bash
# ls -Z /home
auditadm_u:object_r:user_home_dir_t:s0 audadm
secadm_u:object_r:user_home_dir_t:s0 secadm
sysadm_u:object_r:user_home_dir_t:s0 sysadm
```

## 参考

