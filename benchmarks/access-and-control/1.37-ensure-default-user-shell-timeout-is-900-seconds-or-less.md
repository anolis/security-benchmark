# 1.37 确保用户 shell 超时时间小于等于900秒

## 安全等级

- Level 1

## 描述

`TMOUT`是一个环境变量，用于指定`shell`的超时时间，单位为秒。

* `TMOUT=n`   ->   设置shell超时时间为n秒。`TMOUT=0`表示禁用超时时间。
* `readonly TMOUT`   ->   可将`TMOUT`环境变量设置为只读，避免其被篡改。
* `export TMOUT`   ->   对`TMOUT`的值进行修改。

环境变量的配置文件：

* `/etc/profile`   ->   此文件内的变量为全局变量，可作用于所有用户。
* `/etc/profile.d`   ->   在系统启动或用户第一次登录 shell 时，会自动运行其目录下所有`*.sh` 文件。
* `/etc/bashrc`   ->   为每个运行 bash shell 的用户执行该文件，当 bash shell 打开时，该文件被执行，其配置对所有使用 bash 的用户打开的每个 bash 都有效。当被修改后，不用重启系统只需要打开一个新的 bash 即可生效。

设置 shell 超时时间可以减少未经授权的用户通过其他已登录用户的 shell 会话进行非法操作的情况，也能及时释放被不活跃用户占用的会话资源。

## 修复建议

对`TMOUT`环境变量的值进行配置。

1. 检查
* `/etc/bashrc`文件
* `/etc/profile`文件
* `/etc/profile.d/`目录下所有以`.sh`结尾的文件
中`TMOUT=n`条目的值，应不超过`900`且不等于`0`：

示例：
```bash
readonly TMOUT=900 ; export TMOUT
```

或

```bash
TMOUT=900
readonly TMOUT
export TMOUT
```

以上分别为单行与多行配置，符合任意一种即可。

## 扫描检测

确保用户shell超时时间小于等于900秒。

1. 执行以下命令，验证`TMOUT`环境变量是否正确设置：

```bash
#!/usr/bin/env bash
CDTOS()
{
 output1="" output2=""
 [ -f /etc/bashrc ] && BRC="/etc/bashrc"
 for f in "$BRC" /etc/profile /etc/profile.d/*.sh ; do
    grep -Pq '^\s*([^#]+\s+)?TMOUT=(900|[1-8][0-9][0-9]|[1-9][0-9]|[1-9])\b' "$f" && grep -Pq '^\s*([^#]+;\s*)?readonly\s+TMOUT(\s+|\s*;|\s*$|=(900|[1-8][0-9][0-9]|[1-9][0-9]|[1-9]))\b' "$f" && grep -Pq '^\s*([^#]+;\s*)?export\s+TMOUT(\s+|\s*;|\s*$|=(900|[1-8][0-9][0-9]|[1-9][0-9]|[1-9]))\b' "$f" &&  output1="$f"
 done
 grep -Pq '^\s*([^#]+\s+)?TMOUT=(9[0-9][1-9]|9[1-9][0-9]|0+|[1-9]\d{3,})\b' /etc/profile /etc/profile.d/*.sh "$BRC" && output2=$(grep -Ps '^\s*([^#]+\s+)?TMOUT=(9[0-9][1-9]|9[1-9][0-9]|0+|[1-9]\d{3,})\b' /etc/profile /etc/profile.d/*.sh $BRC)
 if [ -n "$output1" ] && [ -z "$output2" ]; then
    echo -e "\nPASSED\n\nTMOUT is configured in: \"$output1\"\n"
 else
    [ -z "$output1" ] && echo -e "\nFAILED\n\nTMOUT is not configured\n"
    [ -n "$output2" ] && echo -e "\nFAILED\n\nTMOUT is incorrectly configured in: \"$output2\"\n"
 fi
}
CDTOS
```

输出：

```bash
PASSED

TMOUT is configured in: "/etc/profile.d/login.sh"
```

如最输出结果为`PASSED`，则视为通过此项检查。其中`TMOUT is configured in`的值为`TMOUT`环境变量所在文件路径。

## 参考

