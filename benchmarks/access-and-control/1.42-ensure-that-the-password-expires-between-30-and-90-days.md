# 1.42 确保密码过期时间在 30 至 90 天之间

## 安全等级

- Level 1

## 描述

`/etc/login.defs`文件中的`PASS_MAX_DAYS`参数，指定了密码过期时间的最大值（天）。建议最大值在 30 至 90 天之间。

## 修复建议

修改`/etc/login.defs`文件中的`PASS_MAX_DAYS`参数。

1. 执行以下代码，修改或添加`/etc/login.defs`文件中的`PASS_MAX_DAYS`参数，使其符合安全要求。

```bash
PASS_MAX_DAYS 90
```

`PASS_MAX_DAYS`默认为`99999`，应修改为`30`至`90`之间。

2. 将密码过期策略应用于所有用户：

```bash
# getent passwd | cut -f1 -d ":" | xargs -n1 chage --maxdays 90
```

## 扫描检测

确保密码过期时间在 30 至 90 天之间。

1. 执行以下命令，验证`/etc/login.defs`文件中的`PASS_MAX_DAYS`参数是否符合要求：

```bash
# grep PASS_MAX_DAYS /etc/login.defs
PASS_MAX_DAYS 90
```

2. 检查用户列表，确认所有用户的密码过期策略符合要求：

```bash
# grep -E '^[^:]+:[^!*]' /etc/shadow | cut -d: -f1,5
<user>:<PASS_MAX_DAYS>
```

* 其中`<user>`为用户名，`<PASS_MAX_DAYS>`为密码过期天数，如：`root:90`。

如配置文件及所有用户的密码过期时间最大值均在 30 至 90 天之间，则视为通过此项检查。

## 参考
