# 1.1 确保 cron 守护进程正常启用

## 安全等级

- Level 1

## 描述

cron 守护进程用于在系统上执行批处理作业。

即使操作系统目前可能没有需要运行的用户作业，也会有系统作业需要运行，其中就可能包括安全监控等重要作业，而 cron 守护进程就是用来执行这些作业的。

## 修复建议

目标：确保 cron 守护进程正常启用。

1. 执行以下命令来启用 cron 进程：

```bash
# systemctl --now enable crond
```

## 扫描检测

确保 cron 守护进程正常启用。

1. 执行以下命令来确定 cron 守护进程是否正常启用：

```bash
# systemctl is-enabled crond
enabled
```

如结果为`enabled`，则视为通过此项检查。

## 参考

