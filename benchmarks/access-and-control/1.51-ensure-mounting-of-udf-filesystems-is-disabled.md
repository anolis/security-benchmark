# 1.51 确保udf文件系统的挂载被禁用

## 安全等级

- Level 1

## 描述

udf文件系统类型是用于实现ISO/IEC 13346和ECMA-167规范的通用磁盘格式。这是一种开放的供应商文件系统类型，用于在广泛的媒体上存储数据。该文件系统类型是必需的，以支持编写DVD和较新的光盘格式。

删除不需要的文件系统类型的支持可以减少系统的本地攻击面。如果不需要此文件系统类型，请禁用它。

## 修复建议

目标：确保udf文件系统的挂载被禁用。

1. 执行以下命令，在`/etc/modprobe.d/`目录中编辑或创建一个以`.conf`结尾的文件，并添加配置。

```bash
# echo "install udf /bin/false" >> /etc/modprobe.d/udf.conf
# echo "blacklist udf" >> /etc/modprobe.d/udf.conf
```

2. 运行以下命令以卸载udf模块。

```bash
# modprobe -r udf
```

## 扫描检测

运行以下命令并验证输出是否符合预期。

1. 模块将如何加载。

   运行如下命令，若输出为`install /bin/false`，则视为通过此项检查。

```bash
# modprobe -n -v udf | grep "^install"
install /bin/false
```

2. 模块当前是否已加载。

   运行如下命令，若输出为空，则视为通过此项检查。

```bash
# lsmod | grep udf
<No  output>
```

3. 模块是否已列入黑名单。

   运行如下命令，若输出为`/etc/modprobe.d/udf.conf:blacklist udf`，则视为通过此项检查。

```bash
# grep -E "^blacklist[[:blank:]]*udf" /etc/modprobe.d/*
/etc/modprobe.d/udf.conf:blacklist     udf
```

## 参考

