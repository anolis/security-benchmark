# 2.22 确保收集对系统管理范围（sudoers）的更改

## 安全等级

- Level 3

## 描述

对系统管理范围（sudoers）的更改操作进行审计记录

## 修复建议

目标：确保收集对系统管理范围（sudoers）的更改。

运行以下命令，配置审计服务，确保收集对系统管理范围（sudoers）的更改：

```bash
# echo -e "-w /etc/sudoers -p wa -k scope\n-w /etc/sudoers.d -p wa -k scope" >> /etc/audit/rules.d/audit.rules
```

执行以下命令，加载审计规则
```bash
# augenrules --load
```

## 扫描检测

确保收集对系统管理范围（sudoers）的更改。

1. 执行以下命令，检查对系统管理范围（sudoers）的审计收集是否正确配置：

```bash
# grep -E "\-w\s/etc/sudoers\s\-p\swa\s\-k\sscope
\-w\s/etc/sudoers.d\s\-p\swa\s\-k\sscope" /etc/audit/rules.d/*.rules /etc/audit/*.rules

/etc/audit/rules.d/audit.rules:-w /etc/sudoers -p wa -k scope
/etc/audit/rules.d/audit.rules:-w /etc/sudoers.d/ -p wa -k scope
/etc/audit/audit.rules:-w /etc/sudoers -p wa -k scope
/etc/audit/audit.rules:-w /etc/sudoers.d/ -p wa -k scope
```

2. 执行以下命令，检查对系统管理范围（sudoers）的审计收集是否正确运行：
```bash
# auditctl -l | grep -P "^\-w\s+\/etc\/sudoers\s+\-p\s+wa\s+\-k\s+scope" && auditctl -l | grep -P "^\-w\s+\/etc\/sudoers.d\s+\-p\s+wa\s+\-k\s+scope"
-w /etc/sudoers -p wa -k scope
-w /etc/sudoers.d -p wa -k scope
```

如输出结果符合预期，则视为通过此项检查。
## 参考