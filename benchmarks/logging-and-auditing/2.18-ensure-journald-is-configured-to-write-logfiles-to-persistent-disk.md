# 2.18 确保 journald 日志文件写入硬盘功能正确开启

## 安全等级

- Level 1

## 描述

journald 的日志数据既可以保存在内存中，也可以保存在本地硬盘中。
内存中的日志在系统崩溃或重启后将会丢失。将日志数据写入硬盘，以保证数据的安全性。

## 修复建议

目标：正确配置 journald 日志写入硬盘功能。

1. 编辑`/etc/systemd/journald.conf`文件，增加如下代码：

```bash
Storage=persistent
```

## 扫描检测

确认 journald 日志写入硬盘功能正确配置

1. 检查`/etc/systemd/journald.conf`文件，确认 journald 日志写入硬盘功能被正确配置。

```bash
# grep -e ^\s*Storage /etc/systemd/journald.conf
Storage=persistent
```

## 参考

