# 4.38 确保 /etc/passwd 中所有组都存在于 /etc/group 中

## 安全等级

- Level 2

## 描述

随着时间的推移，系统管理员的失误或更改可能导致有些在`/etc/passwd`中定义的组，在`/etc/group`中没有。

这种情况会对系统安全造成威胁，因为这种组的权限没有得到正确的监管与配置。

## 修复建议

1. 对出现异常的组，根据实际情况进行修复，如删除或重新正确配置。

## 扫描检测

确保`/etc/passwd`中所有组都存在于`/etc/group`中。

1. 执行以下脚本，检查返回结果：

```bash
#!/bin/bash
for i in $(cut -s -d: -f4 /etc/passwd | sort -u ); do
    grep -q -P "^.*?:[^:]*:$i:" /etc/group
    if [ $? -ne 0 ]; then
    echo "Group $i is referenced by /etc/passwd but does not exist in /etc/group"
    fi
done
```

如没有任何返回值，则视为通过此项检查。


## 参考

