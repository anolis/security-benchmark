# 4.33 确保用户的 dot 文件权限配置正确

## 安全等级

- Level 1

## 描述

在 Linux 下，各种软件的配置文件大多存储于以`.`开头以`rc`结尾的文件中并存放于用户的主目录`~/`中，也就是俗称的 dotfile 者 rcfile，例如 zsh 的配置文件`.zshrc`，vim 的配置文件`.vimrc`等等。

如果用户的 dotfile 权限对 group 与 other 可写，可能使恶意用户窃取或修改其他用户的配置数据。

## 修复建议

* 在不通知用户的情况下对用户的 dotfile 进行全局修改可能会导致程序意外中断和用户不满。因此建议建立监控策略，及时上报用户 dotfile 权限，并根据实际情况，判断采取的措施。

1. 执行以下脚本，删除用户 dotfile 多余的权限：

```bash
#!/bin/bash
awk -F: '($1!~/(halt|sync|shutdown|nfsnobody)/ && $7!~/^(\/usr)?\/sbin\/nologin(\/)?$/ && $7!~/(\/usr)?\/bin\/false(\/)?$/) { print $6 }' /etc/passwd | while read -r dir; do
if [ -d "$dir" ]; then
    for file in "$dir"/.*; do
        if [ ! -h "$file" ] && [ -f "$file" ]; then
            fileperm=$(stat -L -c "%A" "$file")
            if [ "$(echo "$fileperm" | cut -c6)" != "-" ] || [ "$(echo "$fileperm" | cut -c9)" != "-" ]; then
                chmod go-w "$file"
            fi
        fi
    done
fi
done
```

## 扫描检测

确保用户的 dot 文件权限配置正确。

1. 执行以下脚本，检查返回结果：

```bash
#!/bin/bash
awk -F: '($1!~/(halt|sync|shutdown|nfsnobody)/ && $7!~/^(\/usr)?\/sbin\/nologin(\/)?$/ && $7!~/(\/usr)?\/bin\/false(\/)?$/) { print $1 " " $6 }' /etc/passwd | while read -r user dir; do
if [ -d "$dir" ]; then
    for file in "$dir"/.*; do
        if [ ! -h "$file" ] && [ -f "$file" ]; then
            fileperm=$(stat -L -c "%A" "$file")
            if [ "$(echo "$fileperm" | cut -c6)" != "-" ] || [ "$(echo "$fileperm" | cut -c9)" != "-" ]; then
                echo "User: \"$user\" file: \"$file\" has permissions: \"$fileperm\""
            fi
        fi
    done
fi
done
```

如没有任何返回值，则视为通过此项检查。


## 参考

