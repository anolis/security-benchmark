# 4.30 确保 UID 为 0 的用户只有 root

## 安全等级

- Level 2

## 描述

在 Linux 操作系统中，任何 UID 为 0 的用户，都具有超级用户权限。

需确保系统中，只有 root 用户的 UID 为 0。

## 修复建议

检查`/etc/passwd`文件中所有用户的 UID：删除除 root 用户以外，所有 UID 为 0 的用户，或为他们分配一个新的 ID。

例：

```bash
# cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
```

`/etc/passwd`文件格式：`用户名:密码:UID:GID:描述信息:主目录:默认shell`

## 扫描检测

确保 UID 为 0 的用户只有 root。

1. 执行以下命令，检查返回结果：

```bash
# awk -F: '($3 == 0) { print $1 }' /etc/passwd
root
```

如返回结果为`root`，则视为通过此项检查。


## 参考

