# 4.52 确保对可疑报文进行日志记录

## 安全等级

- Level 1

## 描述

启用该特性后，系统会将源地址不可达的报文记录到内核日志。

启用此功能并记录这些数据包，能够使管理员了解和防范攻击者向系统发送欺骗数据包。

## 修复建议

开启可疑报文日志记录功能。

1. 修改`/etc/sysctl.conf`文件及`/etc/sysctl.d`路径下所有后缀为`.conf`文件中以下参数的值。如没有以下参数，则需在`/etc/sysctl.conf`文件中添加：

```bash
net.ipv4.conf.all.log_martians = 1
net.ipv4.conf.default.log_martians = 1
```

2. 执行以下命令，设置活动内核参数：

```bash
# sysctl -w net.ipv4.conf.all.log_martians=1
# sysctl -w net.ipv4.conf.default.log_martians=1
# sysctl -w net.ipv4.route.flush=1
```

## 扫描检测

确保对可疑报文进行日志记录。

1. 执行以下命令，检查返回结果：

```bash
# sysctl net.ipv4.conf.all.log_martians
net.ipv4.conf.all.log_martians = 1
# sysctl net.ipv4.conf.default.log_martians
net.ipv4.conf.default.log_martians = 1
# grep "net\.ipv4\.conf\.all\.log_martians" /etc/sysctl.conf /etc/sysctl.d/*
net.ipv4.conf.all.log_martians = 1
# grep "net\.ipv4\.conf\.default\.log_martians" /etc/sysctl.conf /etc/sysctl.d/*
net.ipv4.conf.default.log_martians = 1
```

如返回结果均符合预期，则视为通过此项检查。


## 参考

