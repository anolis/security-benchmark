# 3.6 禁用LDAP Server

## 安全等级

- Level 1

## 描述

轻量级目录访问协议 (LDAP) 被引入作为 NIS/YP 的替代品。 它是一种提供从中央数据库查找信息的方法的服务。

如果系统不需要充当 LDAP Server，建议禁用LDAP Server以减少潜在的攻击面。

## 修复建议

运行以下命令来禁用`slapd`

```bash
# systemctl --now disable slapd
```

## 扫描检测

运行以下命令来检查`slapd`是否被禁用

```bash
# systemctl is-enabled slapd
```

期待的输出结果**不是**`enabled`

## 参考

