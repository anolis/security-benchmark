# 3.22 确保NIS客户端被卸载

## 安全等级

- Level 1

## 描述

Network Information Service（NIS）是一种采用客户端-服务器架构的目录服务协议，早期也被称为“黄页”服务。它用于分发系统配置文件。NIS客户端（ypbind）用于将设备连接到NIS服务器，并从服务器获取分发下来的配置文件。 

NIS服务从本质上讲是一个不安全的系统，它很容易遭到DOS攻击、缓冲区溢出攻击等，而其用于查询NIS目录的身份认证机制也不可靠。一般来说，NIS服务已经被轻量级目录访问协议（LDAP）等替代。建议将其卸载。

## 修复建议

目标：确保ypbind被卸载

1. 运行以下命令卸载 ypbind。

```bash
# dnf remove -y ypbind
```

## 扫描检测

执行修复前检测ypbind是否安装

1. 运行以下命令以检测是否安装 ypbind。

```bash
# rpm -q ypbind
package ypbind is not installed
```

输出结果为`package ypbind is not installed`则表示未安装ypbind。

## 参考

