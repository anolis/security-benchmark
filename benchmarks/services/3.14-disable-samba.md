# 3.14 禁用Samba

## 安全等级

- Level 1

## 描述

Samba 守护程序允许系统管理员配置他们的 Linux 系统以与 Windows 桌面共享文件系统和目录。 Samba 将通过服务器消息块 (SMB) 协议公布文件系统和目录。 Windows 桌面用户将能够将这些目录和文件系统作为盘符挂载在他们的系统上。

如果不需要将目录和文件系统挂载到 Windows 系统，则可以删除该服务以减少潜在的攻击面。

## 修复建议

运行以下命令来禁用`smb`

```bash
# systemctl --now disable smb
```

## 扫描检测

运行以下命令来检查`smb`是否被禁用

```bash
# systemctl is-enabled smb
```

期待的输出结果**不是**`enabled`

## 参考

