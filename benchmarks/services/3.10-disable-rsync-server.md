# 3.10 禁用Rsync Server

## 安全等级

- Level 1

## 描述

rsyncd 服务可用于在网络连接的系统之间同步文件。

rsyncd 服务存在安全风险，因为它使用未加密的协议进行通信。

## 修复建议

运行以下命令来禁用`rsyncd`

```bash
# systemctl --now disable rsyncd
```

## 扫描检测

运行以下命令来检查`rsyncd`是否被禁用

```bash
# systemctl is-enabled rsyncd
```

期待的输出结果**不是**`enabled`

## 参考

