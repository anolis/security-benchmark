# 3.24 禁用ntalk

## 安全等级

- Level 1

## 描述

talk/ntalk是一个用于Linux用户之间交流的程序，write也可以实现用户交流，但是write一次只能发送一条信息。而talk是基于socket实现的，用户可以实时交流。

因ntalk使用明文传输，且没有密钥的机制，有极大的安全隐患。所以应当禁用ntalk服务，使用更加安全的远程链接方式，如SSH等。

## 修复建议

运行以下命令来禁用`ntalk`

```bash
# systemctl --now disable ntalk
```

## 扫描检测

运行以下命令来检查`ntalk`是否被禁用

```bash
# systemctl is-enabled ntalk
disabled
```

如输出结果为`disabled`，或提示未安装此服务，则视为通过此项检查。

## 参考
