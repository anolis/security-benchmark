# security-benchmark

security-benchmark 是龙蜥下游各个厂商结合自己在安全合规/加固领域的大规模产品落地经验和实践打造的龙蜥社区最佳安全加固实践指南，它包括安全基线（benchmark）、扫描脚本、修复脚本、安全合规镜像制作、安全合规监控等多个方面。其中，Anolis OS 8 、Anolis OS 23 及其最佳安全基线已经完成与[OpenSCAP国际知名社区](https://github.com/ComplianceAsCode/content)的映射与适配，并被OpenSCAP社区合入，详见：
- [OpenSCAP社区Anolis OS 8 standard.profile](https://github.com/ComplianceAsCode/content/blob/master/products/anolis8/profiles/standard.profile)
- [OpenSCAP社区Anolis OS 23 standard.profile](https://github.com/ComplianceAsCode/content/blob/master/products/anolis23/profiles/standard.profile)


#### 介绍

Gitee 是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用 Gitee 实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)

#### 参与贡献

请参考[development-guide](https://gitee.com/anolis/security-benchmark/blob/master/docs/development-guide.md)和以下gitee贡献步骤来贡献您的代码。
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
