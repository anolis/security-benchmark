#!/usr/bin/bash

grep -Eiq '^\s*UsePAM\s+yes' /etc/ssh/sshd_config || echo "UsePAM yes" >> /etc/ssh/sshd_config

systemctl restart sshd