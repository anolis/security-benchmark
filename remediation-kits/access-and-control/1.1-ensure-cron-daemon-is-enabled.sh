#!/usr/bin/bash

result=$(systemctl is-enabled crond)

if [[ $result == "masked" ]] ; then
    systemctl --now unmask crond
    systemctl --now enable crond
elif [[ $result == "disabled" ]] ; then
    systemctl --now enable crond
fi