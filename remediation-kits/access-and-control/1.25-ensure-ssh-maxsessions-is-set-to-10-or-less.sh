#!/usr/bin/bash

maxSessions=$(grep -iP "^(\s*)MaxSessions\s+" /etc/ssh/sshd_config)
maxSessionsNum=$(grep -iP "^(\s*)MaxSessions\s+" /etc/ssh/sshd_config | awk '{print $2}')

if [[ -z $maxSessions && -z $maxSessionsNum ]] ; then
    echo "MaxSessions 10" >> /etc/ssh/sshd_config
elif [[ -n $maxSessions && $maxSessionsNum -gt 10 ]] ; then
    sed -ri 's/^(\s*)MaxSessions\s+.*/MaxSessions 10/g' /etc/ssh/sshd_config
fi

systemctl restart sshd
