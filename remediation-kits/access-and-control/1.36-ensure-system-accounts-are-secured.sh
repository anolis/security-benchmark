#!/usr/bin/bash

# /usr/sbin/nologin与/sbin/nologin文件效果一致，配置其中任意一个即可
awk -F: '($1!="root" && $1!="sync" && $1!="shutdown" && $1!="halt" && $1!~/^\+/ && $3<'"$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)"' && $7!="'"$(which nologin)"'" && $7!="/sbin/nologin" && $7!="/usr/sbin/nologin" && $7!="/bin/false") {print $1}' /etc/passwd | 
while read user; do
    usermod -s $(which nologin) $user
done

awk -F: '($1!="root" && $1!~/^\+/ && $3<'"$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)"') {print $1}' /etc/passwd | xargs -I '{}' passwd -S '{}' | awk '($2!="L" && $2!="LK") {print $1}' | 
while read user; do
    usermod -L $user
done