#!/usr/bin/bash

# 仅对/etc/login.defs /etc/profile* /etc/bashrc*中的umask值进行加固修复，不修改PAM文件中的pam_umask.so

repairFile=$(grep -RPHi '(^|^[^#]*)\s*umask\s+([0-7][0-7][01][0-7]\b|[0-7][0-7][0-7][0-6]\b|[0-7][01][0-7]\b|[0-7][0-7][0-6]\b|(u=[rwx]{0,3},)?(g=[rwx]{0,3},)?o=[rwx]+\b|(u=[rwx]{1,3},)?g=[^rx]{1,3}(,o=[rwx]{0,3})?\b)' /etc/login.defs /etc/profile* /etc/bashrc* | cut -d: -f1 | sort -u)

# 判断是否有需要修复的文件，如没有则跳过修复步骤
if [[ -n $repairFile ]]; then
    for file_name in $repairFile; do
        for line_num in $(grep -PrHin "^(umask|\h+umask)" $file_name | awk -F: '{gsub(/^[ \t]+/, "", $2); print $2}'); do
            # 获取umask的值，保留原本大小写格式
            rep_text=$(sed -n "${line_num}p" $file_name | grep -io umask)
            sed -i "${line_num}s/^\( *\)${rep_text}.*/\1${rep_text} 027/" $file_name
        done
    done
fi

# 判断，如果没有任何umask配置，则直接追加/etc/profile末尾
grep -RPiq '(^[^#]*\s*umask\s+([0-7]{3})\b|\bumask\s+u=([rwx]*),g=([rwx]*),o=([rwx]*))' /etc/bashrc* /etc/profile* /etc/login.defs || echo "umask 027" >> /etc/profile