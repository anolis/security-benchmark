#!/usr/bin/bash

rootGid=$(grep "^root:" /etc/passwd | cut -f4 -d:)

[[ $rootGid != 0 ]] && usermod -g 0 root