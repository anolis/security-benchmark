#!/usr/bin/bash

sshLogLevelCount=$(grep -icP "Loglevel\s+.*" /etc/ssh/sshd_config)
sshLogLevel=$(grep -iP "Loglevel\s+.*" /etc/ssh/sshd_config)
sshLogLevelNum=$(grep -iPn "Loglevel\s+.*" /etc/ssh/sshd_config | cut -d: -f1)

[[ $sshLogLevelCount -gt 1 ]] && exit 1

if [[ -z $sshLogLevel ]] ; then
    echo "LogLevel INFO" >> /etc/ssh/sshd_config
else
    sed -i "$sshLogLevelNum"d /etc/ssh/sshd_config
    echo "LogLevel INFO" >> /etc/ssh/sshd_config
fi

systemctl restart sshd