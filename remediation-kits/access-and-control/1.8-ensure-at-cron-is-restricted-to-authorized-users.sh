#!/usr/bin/bash

[[ -e /etc/cron.deny ]] && rm -f /etc/cron.deny
[[ -e /etc/at.deny ]] && rm -f /etc/at.deny
[[ ! -e /etc/cron.allow ]] && touch /etc/cron.allow
[[ ! -e /etc/at.allow ]] && touch /etc/at.allow
[[ -e /etc/cron.allow ]] && chmod og-rwx /etc/cron.allow && chmod og-rwx /etc/at.allow && chown root:root /etc/cron.allow && chown root:root /etc/at.allow