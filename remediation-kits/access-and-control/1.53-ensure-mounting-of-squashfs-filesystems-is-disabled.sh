#!/usr/bin/bash

grep -Psq "^install\s+squashfs\s+\/bin\/false$" /etc/modprobe.d/*.conf || echo "install squashfs /bin/false" >> /etc/modprobe.d/squashfs.conf
grep -Psq "^blacklist\s+squashfs$" /etc/modprobe.d/*.conf || echo "blacklist squashfs" >> /etc/modprobe.d/squashfs.conf 
modprobe -r squashfs