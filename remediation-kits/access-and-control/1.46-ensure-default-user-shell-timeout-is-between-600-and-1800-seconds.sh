#!/usr/bin/bash

[ -f /etc/bashrc ] && BRC="/etc/bashrc"
for f in "$BRC" /etc/profile /etc/profile.d/*.sh ; do
    val_TMOUT=$(grep -vP "^#.*" $f | grep -Pio "TMOUT=[0-9]+" | tail -1 | cut -d"=" -f 2)
    [[ -n $val_TMOUT ]] && ex_TMOUT=true
    if [[ -n $val_TMOUT ]] && [[ $val_TMOUT -lt 600 || $val_TMOUT -gt 1800 ]] ; then
        sed -ri s/"TMOUT=[0-9]+"/"TMOUT=900"/ $f
    else
        :
    fi
done

[[ $ex_TMOUT != "true" ]] && echo "readonly TMOUT=900 ; export TMOUT" >> /etc/profile