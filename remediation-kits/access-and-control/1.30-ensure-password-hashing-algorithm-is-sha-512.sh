#!/usr/bin/bash


authselect check &> /dev/null && auCheck=0

if [[ $auCheck == "0" ]] ; then
    customProfile=$(authselect current | awk 'NR == 1 {print $3}' | grep custom/)
    currProfile=$(authselect current | awk 'NR == 1 {print $3}')
    createP="user-profile"

    # 判断是否已有用户自定义规则集，如有则跳过创建步骤，改为直接追加已有的规则集
    if [[ -z $customProfile ]]; then
        authselect create-profile $createP -b $currProfile
        authselect select custom/$createP
        authselect apply-changes
    fi
fi


[[ $auCheck == "0" ]] && customProfile=$(authselect current | awk 'NR == 1 {print $3}' | grep custom/) || customProfile=""
# 判断是否有自定义规则集，如没有或创建失败，则直接对最终文件(/etc/pam.d/password-auth,/etc/pam.d/system-auth)进行修改
for FN in system-auth password-auth; do
    [[ -n $customProfile ]] && PTF=/etc/authselect/$customProfile/$FN || PTF=/etc/pam.d/$FN
    [[ -z $(grep -E '^\s*password\s+sufficient\s+pam_unix.so.*$' $PTF) ]] && ln=$(grep -En -m1 '^\s*password\s+' $PTF | cut -d: -f1) && sed -i "${ln}i\password    sufficient    pam_unix.so " $PTF

    hashLN=$(grep -En '^\s*password\s+.*pam_unix.so\s*.*' $PTF | grep -Pv "(sha[0-9]+|md5)" | cut -d: -f1)
    if [[ -n $hashLN ]] ; then
        for l in $hashLN; do sed -ri "${l}s/(^\s*password\s+)(\s+sufficient\s+)(\s+pam_unix\.so\b\s*)(.*)$/\1\2\3 sha512 \4/" $PTF  ; done
    elif [[ -n $(grep -E '^\s*password\s+sufficient\s+pam_unix.so\s*.*(sha[0-9]+|md5)\s*.*$' $PTF | grep -Pv "sha512") ]] ;then
        sed -ri "/^\s*password\s+sufficient\s+pam_unix.so/s/(sha[0-9]+|md5)/sha512/g" $PTF
    fi

done

[[ $auCheck == "0" ]] && authselect apply-changes &> /dev/null
