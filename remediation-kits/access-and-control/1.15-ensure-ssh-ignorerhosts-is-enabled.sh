#!/usr/bin/bash

IgnoreRhosts=$(grep -E "^(\s*)IgnoreRhosts\s+\S+(\s*#.*)?\s*$" /etc/ssh/sshd_config | awk '{print $2}')

if [[ $IgnoreRhosts == 'no' ]] ; then
    sed -ri "s/^(\s*)IgnoreRhosts\s+\S+(\s*#.*)?\s*$/\1IgnoreRhosts yes\2/" /etc/ssh/sshd_config
fi

systemctl restart sshd