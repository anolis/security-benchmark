#!/usr/bin/bash

# Before running this script, check whether any user whose password has expired. If yes, run this script after handling the problem. Otherwise, the user may be locked

for n in $(getent shadow | cut -d : -f 1,3) ; do
    result=0
    userName=$(echo $n | cut -d : -f 1)
    userChangeTime=$(echo $n | cut -d : -f 2)
    if [[ -z $userChangeTime ]] ; then
        # Determine whether there are aging disabled users
        echo "[$userName]:due to aging disabled,change the password and run this script again." && errorValue=1
    else
        [[ $errorValue != 1 ]] && result=1
    fi
done

[[ $result ]] && [[ $result == 1 ]] && useradd -D -f 30 && getent passwd | cut -f1 -d ":" | xargs -n1 chage --inactive 30