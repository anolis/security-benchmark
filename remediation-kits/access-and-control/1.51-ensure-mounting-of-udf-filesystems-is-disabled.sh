#!/usr/bin/bash

grep -Psq "^install udf /bin/false" /etc/modprobe.d/udf.conf || echo "install udf /bin/false" >> /etc/modprobe.d/udf.conf
grep -Psq "^blacklist udf" /etc/modprobe.d/udf.conf || echo "blacklist udf" >> /etc/modprobe.d/udf.conf
modprobe -r udf
