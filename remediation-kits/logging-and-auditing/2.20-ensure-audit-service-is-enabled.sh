#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [ "$(rpm -qa audit)" ]; then
    result=$(systemctl is-enabled auditd.service)
    if [[ $result == "masked" ]] ; then
        systemctl --now unmask auditd.service
        systemctl --now enable auditd.service
    elif [[ $result == "disabled" ]] ; then
        systemctl --now enable auditd.service
    fi
fi