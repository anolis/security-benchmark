#!/usr/bin/bash

mkdir -p /etc/aide
grep -Psq "^\/sbin\/auditctl p\+i\+n\+u\+g\+s\+b\+acl\+xattrs\+sha512" /etc/aide/aide.conf || echo "/sbin/auditctl p+i+n+u+g+s+b+acl+xattrs+sha512" >> /etc/aide/aide.conf
grep -Psq "^\/sbin\/auditd p\+i\+n\+u\+g\+s\+b\+acl\+xattrs\+sha512" /etc/aide/aide.conf || echo "/sbin/auditd p+i+n+u+g+s+b+acl+xattrs+sha512"  >> /etc/aide/aide.conf
grep -Psq "^\/sbin\/ausearch p\+i\+n\+u\+g\+s\+b\+acl\+xattrs\+sha512" /etc/aide/aide.conf || echo "/sbin/ausearch p+i+n+u+g+s+b+acl+xattrs+sha512" >> /etc/aide/aide.conf
grep -Psq "^\/sbin\/aureport p\+i\+n\+u\+g\+s\+b\+acl\+xattrs\+sha512" /etc/aide/aide.conf || echo "/sbin/aureport p+i+n+u+g+s+b+acl+xattrs+sha512" >> /etc/aide/aide.conf
grep -Psq "^\/sbin\/autrace p\+i\+n\+u\+g\+s\+b\+acl\+xattrs\+sha512" /etc/aide/aide.conf || echo "/sbin/autrace p+i+n+u+g+s+b+acl+xattrs+sha512" >> /etc/aide/aide.conf
grep -Psq "^\/sbin\/augenrules p\+i\+n\+u\+g\+s\+b\+acl\+xattrs\+sha512" /etc/aide/aide.conf || echo "/sbin/augenrules p+i+n+u+g+s+b+acl+xattrs+sha512" >> /etc/aide/aide.conf
