#!/usr/bin/bash

sed -ri /'^LogDenied\=\s*(unicast|broadcast|multicast|off)$'/s/'^LogDenied\=\s*(unicast|broadcast|multicast|off)$'/LogDenied=all/ /etc/firewalld/firewalld.conf

grep -Psq "^LogDenied\=\s*(all|unicast|broadcast|multicast|off)$" /etc/firewalld/firewalld.conf || echo "LogDenied=all" >> /etc/firewalld/firewalld.conf

systemctl restart firewalld.service