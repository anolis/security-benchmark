#!/usr/bin/bash

for BIT in b32 b64 ; do
    checkRule="^(?=^\s*-a\s+always,exit)(?=.*-F\s+arch=$BIT)(?=.*chmod)(?=.*fchmod)(?=.*chown)(?=.*fchown)(?=.*lchown)(?=.*setxattr)(?=.*lsetxattr)(?=.*fsetxattr)(?=.*removexattr)(?=.*lremovexattr)(?=.*fremovexattr)(?=.*fchownat)(?=.*fchmodat)"
    grep -Pq $checkRule /etc/audit/rules.d/*.rules /etc/audit/*.rules || echo "-a always,exit -F arch=$BIT -S chmod,lchown,fchmod,fchown,chown,setxattr,lsetxattr,fsetxattr,removexattr,lremovexattr,fremovexattr,fchownat,fchmodat -F auid>=1000 -F auid!=-1 -F key=perm_mod" >> /etc/audit/rules.d/50-perm_mod.rules
done

augenrules --load