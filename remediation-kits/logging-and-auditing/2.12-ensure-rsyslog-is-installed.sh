#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [ ! "$(rpm -qa rsyslog | grep -i "rsyslog\-")" ]; then
    yum install rsyslog -y
fi
