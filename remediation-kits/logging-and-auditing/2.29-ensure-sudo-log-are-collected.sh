#!/usr/bin/bash

grep -Psq "^\s*Defaults\s+logfile\s*=\s*(/?)([a-zA-Z0-9_.-]+/?)*" /etc/sudoers || echo "Defaults logfile=/var/log/sudo.log" >> /etc/sudoers
