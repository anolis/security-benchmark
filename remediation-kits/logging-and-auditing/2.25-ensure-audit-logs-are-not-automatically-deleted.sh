#!/usr/bin/bash


grep -Psq "^max_log_file_action\s*=.*" /etc/audit/auditd.conf && sed -i 's/^max_log_file_action.*/max_log_file_action = keep_logs/' /etc/audit/auditd.conf || echo "max_log_file_action = keep_logs" >> /etc/audit/auditd.conf

augenrules --load