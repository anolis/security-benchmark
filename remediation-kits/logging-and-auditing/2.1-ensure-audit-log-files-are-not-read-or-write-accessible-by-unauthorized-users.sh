#!/usr/bin/bash

logFile=$(grep -iw log_file /etc/audit/auditd.conf | cut -d= -f2)
logDir=$(dirname $logFile)
[[ $logDir ]] && test -f $logDir/* && chmod 0600 $logDir/*