#!/usr/bin/bash

grep -Psq "^Storage=persistent" /etc/systemd/journald.conf || echo "Storage=persistent" >> /etc/systemd/journald.conf
