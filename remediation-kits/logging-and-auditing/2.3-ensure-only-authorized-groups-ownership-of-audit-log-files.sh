#!/usr/bin/bash

logFile=$(grep -iw log_file /etc/audit/auditd.conf | cut -d= -f2)
logDir=$(dirname $logFile)
[[ $logDir ]] && chown :adm $logDir

sed -i '/^log_group/D' /etc/audit/auditd.conf
sed -i /^log_file/a'log_group = adm' /etc/audit/auditd.conf 
systemctl kill auditd -s SIGHUP