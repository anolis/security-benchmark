#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [ ! "$(rpm -qa audit)" ]; then
    yum install audit -y
elif [ ! "$(rpm -qa audit-libs)" ]; then
    yum install audit-libs -y
fi