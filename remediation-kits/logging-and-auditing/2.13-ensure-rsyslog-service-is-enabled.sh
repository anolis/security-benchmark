#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [ "$(rpm -qa rsyslog)" ]; then
    result=$(systemctl is-enabled rsyslog.service)
    if [[ $result == "masked" ]] ; then
        systemctl --now unmask rsyslog.service
        systemctl --now enable rsyslog.service
    elif [[ $result == "disabled" ]] ; then
        systemctl --now enable rsyslog.service
    fi
fi