#!/usr/bin/bash

grep -Psq "\-w\s+\/var\/log\/lastlog\s+\-p\s+wa\s+(\-k\s+.*)" /etc/audit/rules.d/*.rules || echo -e "-w /var/log/lastlog -p wa -k logins" >> /etc/audit/rules.d/audit-root.rules
grep -Psq "\-w\s+\/var\/run\/faillock\s+\-p\s+wa\s+(\-k\s+.*)" /etc/audit/rules.d/*.rules || echo -e "-w /var/run/faillock -p wa -k logins" >> /etc/audit/rules.d/audit-root.rules

augenrules --load