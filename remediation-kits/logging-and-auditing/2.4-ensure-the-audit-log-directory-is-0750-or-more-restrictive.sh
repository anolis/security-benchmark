#!/usr/bin/bash

logFile=$(grep -iw log_file /etc/audit/auditd.conf | cut -d= -f2)
logDir=$(dirname $logFile)
[[ $logDir ]] && chmod -R g-w,o-rwx $logDir