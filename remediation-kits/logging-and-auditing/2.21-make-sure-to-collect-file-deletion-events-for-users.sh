#!/usr/bin/bash

Rule32="-a always,exit -F arch=b32 -S unlink,rename,unlinkat,renameat -F auid>=1000 -F auid!=-1 -F key=delete"
x86Rule64="-a always,exit -F arch=b64 -S rename,unlink,unlinkat,renameat -F auid>=1000 -F auid!=-1 -F key=delete"

armRule64="-a always,exit -F arch=b64 -S unlinkat,renameat -F auid>=1000 -F auid!=-1 -F key=delete"

checkRule="^(?=^\s*-a\s+always,exit)(?=.*-F\s+arch=b(64|32))(?=.*rename)(?=.*unlink)(?=.*unlinkat)(?=.*renameat)(?=.*-F\s+auid>=1000)"
checkRuleArm="^(?=^\s*-a\s+always,exit)(?=.*-F\s+arch=b(64|32))(?=.*unlinkat)(?=.*renameat)(?=.*-F\s+auid>=1000)"

if [[ $(arch) == 'aarch64' ]] && [[ $(uname -m) == 'aarch64' ]] ; then
    grep -Psq "$checkRuleArm" /etc/audit/rules.d/audit.rules || echo -e "\n$Rule32\n$armRule64\n" >> /etc/audit/rules.d/audit.rules
else
    grep -Psq "$checkRule" /etc/audit/rules.d/audit.rules || echo -e "\n$Rule32\n$x86Rule64\n" >> /etc/audit/rules.d/audit.rules
fi

augenrules --load