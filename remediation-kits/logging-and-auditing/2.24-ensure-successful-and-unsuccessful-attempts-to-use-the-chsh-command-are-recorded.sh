#!/usr/bin/bash

grep -Ps "^(?=^\s*-a\s+always,exit)(?=.*-S\s+all)(?=.*-F\s+path=/usr/bin/chsh)(?=.*-F\s+perm=x)(?=.*-F\s+auid>=1000)(?=.*-F\s+auid!=-1)" /etc/audit/rules.d/*.rules || echo -e "-a always,exit -S all -F path=/usr/bin/chsh -F perm=x -F auid>=1000 -F auid!=-1 -F key=priv_cmd" >> /etc/audit/rules.d/stig.rules
augenrules --load
