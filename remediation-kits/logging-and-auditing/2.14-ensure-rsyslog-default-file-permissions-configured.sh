#!/usr/bin/bash

grep -Psq "^\\\$FileCreateMode 0640" /etc/rsyslog.conf || echo "\$FileCreateMode 0640" >> /etc/rsyslog.conf