#!/usr/bin/bash

rpm -q iptables-services | grep -Psq "^iptables\-services.*" && systemctl is-enabled iptables | grep -Psiq "^enabled" && systemctl --now mask iptables.service