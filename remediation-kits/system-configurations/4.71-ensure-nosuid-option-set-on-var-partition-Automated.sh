#!/usr/bin/bash

if [[ -e /etc/fstab ]] && [[ -n "$(grep -Ps "\s+\/var\s+" /etc/fstab)" ]] && [[ -z "$(grep -Ps "\s+\/var\s+.*nosuid" /etc/fstab)" ]] ; then
    varLine=$(grep -Pn "\s+\/var\s+" /etc/fstab | cut -d: -f1)
    varCon=$(grep "\/var" /etc/fstab | awk '{print $4}')
    sed -ri "${varLine}s/${varCon}/${varCon},nosuid/g" /etc/fstab
    mount -o remount /var
fi