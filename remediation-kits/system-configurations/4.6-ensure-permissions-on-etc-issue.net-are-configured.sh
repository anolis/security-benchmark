#!/usr/bin/bash

[[ -e /etc/issue.net ]] && chown root:root /etc/issue.net
[[ -e /etc/issue.net ]] && chmod u-x,go-wx /etc/issue.net