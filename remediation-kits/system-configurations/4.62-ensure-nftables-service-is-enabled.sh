#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [ "$(rpm -qa nftables)" ]; then
    result=$(systemctl is-enabled nftables)
    if [[ $result == "enabled" ]] ; then
        :
    elif [[ $result == "masked" ]] ; then
        systemctl --now unmask nftables
        systemctl --now enable nftables
    elif [[ $result == "disabled" ]] ; then
        systemctl --now enable nftables
    fi
fi