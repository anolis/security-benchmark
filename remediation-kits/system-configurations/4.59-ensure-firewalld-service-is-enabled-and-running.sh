#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa firewalld) ]]; then
    result=$(systemctl is-enabled firewalld)
    if [[ $result == "masked" ]] ; then
        systemctl --now unmask firewalld
        systemctl --now enable firewalld
    elif [[ $result == "disabled" ]] ; then
        systemctl --now enable firewalld
    elif [[ $result == "enabled" ]] ; then
        [[ $(firewall-cmd --state) ]] || systemctl --now enable firewalld
    fi
fi