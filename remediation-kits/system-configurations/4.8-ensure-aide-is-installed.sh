#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [ ! "$(rpm -qa aide)" ]; then
    dnf install aide -y
    aide --init
    mv /var/lib/aide/aide.db.new.gz /var/lib/aide/aide.db.gz
fi