#!/usr/bin/bash

modprobe -n -q sctp && modprobe -n -v sctp | grep -Pq "^install\s*\/bin\/true\s*$"

if [[ $? -ne 0 ]]; then
    lsmod | grep -Pq "^sctp\b" && rmmod sctp
    echo -e "\ninstall sctp /bin/true" >> /etc/modprobe.d/sctp.conf
fi
