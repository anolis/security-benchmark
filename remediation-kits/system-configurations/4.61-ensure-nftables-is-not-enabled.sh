#!/usr/bin/bash

rpm -q nftables | grep -Psq "^nftables\-*" && systemctl is-enabled nftables | grep -Psiq "^enabled" && systemctl --now mask nftables