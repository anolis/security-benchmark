#!/usr/bin/bash

osID=$(cat /etc/os-release | grep -Pi "^ID=" | cut -f2 -d= | sed -rn "s/\"//gp")

[ -f /boot/grub2/grub.cfg ] && chown root:root /boot/grub2/grub.cfg;
[ -f /boot/grub2/grub.cfg ] && chmod og-rwx /boot/grub2/grub.cfg;
[ -f /boot/grub2/grubenv ] && chown root:root /boot/grub2/grubenv;
[ -f /boot/grub2/grubenv ] && chmod og-rwx /boot/grub2/grubenv;
[ -f /boot/grub2/user.cfg ] && chown root:root /boot/grub2/user.cfg;
[ -f /boot/grub2/user.cfg ] && chmod og-rwx /boot/grub2/user.cfg;
[ -f /boot/efi/EFI/$osID/grubenv ] && chown root:root /boot/efi/EFI/$osID/grubenv
[ -f /boot/efi/EFI/$osID/grubenv ] && chmod og-rwx /boot/efi/EFI/$osID/grubenv