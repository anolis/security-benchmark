#!/usr/bin/bash

[[ -e /etc/issue ]] && chown root:root /etc/issue
[[ -e /etc/issue ]] && chmod u-x,go-wx /etc/issue