#!/usr/bin/bash

grep -Ps "^\s*net\.ipv4\.ip_forward\s*=.*" /etc/sysctl.conf /etc/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /run/sysctl.d/*.conf | grep -Pvs "net.ipv4.ip_forward\s*=\s*0\s*$" | cut -f1 -d: | while read filename; do sed -ri "s/^\s*(net\.ipv4\.ip_forward\s*)(=)(\s*\S+\b).*$/# *REMOVED* \1/" $filename; done; sysctl -w net.ipv4.ip_forward=0; sysctl -w net.ipv4.route.flush=1
grep -Ps "^\s*net\.ipv6\.conf\.all\.forwarding\s*=.*" /etc/sysctl.conf /etc/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /run/sysctl.d/*.conf | grep -Pvs "net.ipv6.conf.all.forwarding\s*=\s*0\s*$" | cut -f1 -d: | while read filename; do sed -ri "s/^\s*(net\.ipv6\.conf\.all\.forwarding\s*)(=)(\s*\S+\b).*$/# *REMOVED* \1/" $filename; done; sysctl -w net.ipv6.conf.all.forwarding=0; sysctl -w net.ipv6.route.flush=1
