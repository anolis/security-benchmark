#!/usr/bin/bash

grep -Ps "^\s*net\.ipv4\.icmp_echo_ignore_broadcasts\s*=.*" /etc/sysctl.conf /etc/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /run/sysctl.d/*.conf | grep -Pvs "net.ipv4.icmp_echo_ignore_broadcasts\s*=\s*1\s*$" | cut -f1 -d: | while read filename; do sed -ri "s/^\s*(net\.ipv4\.icmp_echo_ignore_broadcasts\s*)(=)(\s*\S+\b).*$/# *REMOVED* \1/" $filename; done; sysctl -w net.ipv4.icmp_echo_ignore_broadcasts=1; sysctl -w net.ipv4.route.flush=1
