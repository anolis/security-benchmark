#!/usr/bin/bash

modprobe -n -vq dccp

if [[ $? -ne 0 ]]; then
    echo -e "\ninstall dccp /bin/true" >> /etc/modprobe.d/dccp.conf
fi