#!/usr/bin/bash

rpm -q firewalld | grep -Psq "^firewalld\-" && systemctl is-enabled firewalld | grep -Psiq "^enabled" && systemctl --now mask firewalld