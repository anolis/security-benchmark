#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa vsftpd) ]]; then
    result=$(systemctl is-enabled vsftpd)
    if [[ $result == enabled ]]; then
        systemctl --now disable vsftpd
    fi
fi