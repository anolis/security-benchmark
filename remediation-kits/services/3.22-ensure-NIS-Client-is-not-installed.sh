#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa ypbind) ]]; then
    dnf remove -y ypbind
fi