#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa httpd) ]]; then
    result=$(systemctl is-enabled httpd)
    if [[ $result == enabled ]]; then
        systemctl --now disable httpd
    fi
fi