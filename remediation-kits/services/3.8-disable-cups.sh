#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa cups) ]]; then
    result=$(systemctl is-enabled cups)
    if [[ $result == enabled ]]; then
        systemctl --now disable cups
    fi
fi