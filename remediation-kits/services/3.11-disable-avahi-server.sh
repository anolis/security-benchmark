#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa avahi) ]]; then
    result=$(systemctl is-enabled avahi-daemon.socket)
    result2=$(systemctl is-enabled avahi-daemon)
    [[ $result == enabled ]] && systemctl --now disable avahi-daemon.socket
    [[ $result2 == enabled ]] && systemctl --now disable avahi-daemon.service
fi