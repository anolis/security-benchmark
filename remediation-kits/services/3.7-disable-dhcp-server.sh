#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa | grep dhcp-server) ]]; then
    result=$(systemctl is-enabled dhcpd)
    if [[ $result == enabled ]]; then
        systemctl --now disable dhcpd
    fi
fi
