#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa wpa_supplicant) ]]; then
    yum remove -y --noautoremove wpa_supplicant
fi