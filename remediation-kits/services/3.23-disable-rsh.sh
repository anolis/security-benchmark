#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa rsh) ]]; then
    result=$(systemctl is-enabled rsh.socket)
    if [[ $result == enabled ]]; then
        systemctl --now disable rsh.socket
    fi
fi