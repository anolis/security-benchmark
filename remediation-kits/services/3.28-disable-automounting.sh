#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa autofs) ]]; then
    yum remove -y --noautoremove autofs
    [[ $? != 0 ]] && result=$(systemctl is-enabled autofs.service)
    if [[ $result == enabled ]]; then
        systemctl --now disable autofs
    fi
fi