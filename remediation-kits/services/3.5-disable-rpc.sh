#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa rpcbind) ]]; then
    result=$(systemctl is-enabled rpcbind)
    if [[ $result == enabled ]]; then
        systemctl stop rpcbind.socket
        systemctl mask rpcbind
        systemctl stop rpcbind.service
    fi
fi