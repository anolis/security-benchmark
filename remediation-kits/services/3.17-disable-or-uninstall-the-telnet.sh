#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa | grep telnet-server) ]]; then
    dnf remove -y telnet telnet-server
    [[ $? != 0 ]] && result=$(systemctl is-enabled telnet.socket)
    if [[ $result == enabled ]]; then
        systemctl --now disable telnet.socket
    fi
fi
