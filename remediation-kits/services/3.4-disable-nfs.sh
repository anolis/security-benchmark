#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa nfs-utils) ]]; then
    result=$(systemctl is-enabled nfs-server)
    if [[ $result == enabled ]]; then
        systemctl --now disable nfs-server
    fi
fi