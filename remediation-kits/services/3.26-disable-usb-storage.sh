#!/usr/bin/bash

grep -Psq "^install\s+usb\-storage\s+\/bin\/true$" /etc/modprobe.d/*.conf || echo "install usb-storage /bin/true" >> /etc/modprobe.d/usb_storage.conf
[[ $(lsmod | grep -P "^usb(_|-)storage\b") ]] && rmmod usb-storage
