#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa net-snmp) ]]; then
    result=$(systemctl is-enabled snmpd)
    if [[ $result == enabled ]]; then
        systemctl --now disable snmpd
    fi
fi