#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa ypserv) ]]; then
    result=$(systemctl is-enabled ypserv)
    if [[ $result == enabled ]]; then
        systemctl --now disable ypserv
    fi
fi