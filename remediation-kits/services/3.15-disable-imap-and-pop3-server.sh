#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa dovecot) ]]; then
    result=$(systemctl is-enabled dovecot)
    if [[ $result == enabled ]]; then
        systemctl --now disable dovecot
    fi
fi