#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa | grep firstboot) ]]; then
    yum remove -y --noautoremove firstboot
fi