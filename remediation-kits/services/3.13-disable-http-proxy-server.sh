#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa squid) ]]; then
    result=$(systemctl is-enabled squid)
    if [[ $result == enabled ]]; then
        systemctl --now disable squid
    fi
fi