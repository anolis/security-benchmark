#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa postfix) ]]; then
    result=$(systemctl is-enabled postfix.service)
    if [[ $result == enabled ]]; then
        systemctl --now disable postfix.service
    fi
fi
