#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ "$(rpm -qa bind)" ]]; then
    result=$(systemctl is-enabled named)
    if [[ $result == enabled ]]; then
        systemctl --now disable named
    fi
fi