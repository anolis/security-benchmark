#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa | grep rsync) ]]; then
    [[ $(systemctl list-unit-files | grep rsyncd) ]] && result=$(systemctl is-enabled rsyncd)
    if [[ $result == enabled ]]; then
        systemctl --now disable rsyncd
    fi
fi