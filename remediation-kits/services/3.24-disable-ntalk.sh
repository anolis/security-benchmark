#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa ntalk) ]]; then
    result=$(systemctl is-enabled ntalk.socket)
    if [[ $result == enabled ]]; then
        systemctl --now disable ntalk.socket
    fi
fi