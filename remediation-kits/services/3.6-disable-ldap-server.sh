#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa openldap-servers) ]]; then
    result=$(systemctl is-enabled slapd)
    if [[ $result == enabled ]]; then
        systemctl --now disable slapd
    fi
fi