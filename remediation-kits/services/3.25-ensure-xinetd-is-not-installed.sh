#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa xinetd) ]]; then
    dnf remove -y xinetd
fi