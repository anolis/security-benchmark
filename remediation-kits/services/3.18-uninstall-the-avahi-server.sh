#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa avahi) ]]; then
    yum remove -y --noautoremove avahi
fi