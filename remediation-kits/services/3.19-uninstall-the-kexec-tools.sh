#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa | grep kexec) ]]; then
    yum remove -y --noautoremove kexec-tools
fi