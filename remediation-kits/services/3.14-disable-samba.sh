#!/usr/bin/bash

export LANG="en_US.UTF-8"

if [[ $(rpm -qa samba) ]]; then
    result=$(systemctl is-enabled smb)
    if [[ $result == enabled ]]; then
        systemctl --now disable smb
    fi
fi