%define version v1.5.1
%define anolis_release 1

Name: security-benchmark
Version: %{version}
Release: %{anolis_release}%{?dist}
Summary: security-benchmark
Group: Development/Tools

License: MulanPSL-2.0
URL: https://gitee.com/anolis/security-benchmark
Source: %{name}-%{version}.tar.gz

BuildArch: noarch

Requires: bc

%description
Security-benchmark home: https://gitee.com/anolis/security-benchmark
Download address of release 1.5.0: https://gitee.com/anolis/security-benchmark/archive/refs/tags/v1.5.1.tar.gz

%prep
%setup -q

%build

%install
install -d -p %{buildroot}%{_datadir}/%{name}/tools/scanners
install -d -p %{buildroot}%{_datadir}/%{name}/tools/remediation-kits
install -d -p %{buildroot}%{_datadir}/%{name}/scanners
install -d -p %{buildroot}%{_datadir}/%{name}/remediation-kits

cp -a tools/scanners %{buildroot}%{_datadir}/%{name}/tools
cp -a tools/remediation-kits %{buildroot}%{_datadir}/%{name}/tools
cp -a scanners %{buildroot}%{_datadir}/%{name}
cp -a remediation-kits %{buildroot}%{_datadir}/%{name}

%files
%defattr (-,root,root,0755)
%license LICENSE
%{_datadir}/%{name}/tools/scanners
%{_datadir}/%{name}/tools/remediation-kits
%{_datadir}/%{name}/scanners
%{_datadir}/%{name}/remediation-kits

%changelog
* Fri Jan 19 Yuqing Yang <yyq01323329@alibaba-inc.com> - 1.5.1-1
- Bump the version to v1.5.1.

* Tue Dec 5 Yuqing Yang <yyq01323329@alibaba-inc.com> - 1.5.0-1
- Bump the version to v1.5.0.

* Tue May 30 Yuqing Yang <yyq01323329@alibaba-inc.com> - 1.4.0-1
- Bump the version to v1.4.0.

* Tue Apr 4 2023 Yilin Li <YiLin.Li@linux.alibaba.com> - 1.3.0-2
- fix some bugs and change to install in %{_datadir}.

* Mon Apr 3 2023 Yuqing Yang <yyq01323329@alibaba-inc.com> - 1.3.0-1
- Updated version.

* Fri Mar 31 2023 Yuqing Yang <yyq01323329@alibaba-inc.com> - 1.2.0-2
- Add Requires bc.

* Wed Mar 29 2023 Yuqing Yang <yyq01323329@alibaba-inc.com> - 1.2.0-1
- Init package.
