# 1、简介
1. 此工具适用于 Anolis OS 操作系统

2. 自动调用“龙蜥最佳安全基线”中加固脚本，对系统进行安全加固

3. 可使用预设配置或按自身需求自由选择加固项目


# 2、文件及目录说明

- Anolis_security_benchmark_level1.config -- 配置文件，用于存储待加固的项目编号

- Reference_DengBaoThree.config -- 此 config 文件包含的编号为：已发布的 benchmark 中参考了 等保2.0三级 标准的规则。其中包含 level 3 的规则，需谨慎使用

- run_Anolis_remediation_kit.sh -- 可执行文件，用于调用加固脚本对系统进行加固

- config（目录） -- 用于存放config文件

- log（目录） -- 保存每次执行加固脚本后的日志文件


# 3、使用方法

## 3.1 确认系统环境

此工具适用于 Anolis OS 操作系统

```shell
# cat /etc/redhat-release
Anolis OS release ...
```

![image-20230328153718574](./img/系统版本.png)

- 此工具已在 Anolis OS 8 、 Anolis OS 23 下进行了完整测试。
- 在 v1.3.0 版本更新中，已移除了系统版本检测，目前此工具可在所有Linux发行版下执行。
- 在非 Anolis OS 系统执行此工具前请仔细阅读本文，并确认即将执行的加固脚本对系统环境的影响，务必谨慎操作。

## 3.2 确认加固项目

默认使用config文件夹下的`Anolis_security_benchmark_level1.config`中的加固项目

也可根据使用场景，自行选择`security-benchmark/remediation-kits`目录下的加固项目

## 3.3 执行自动化工具

1. 自动化工具路径：`security-benchmark/tools/remediation-kits`

2. 自定义config文件：
config文件建议存放在`security-benchmark/tools/remediation-kits/config`目录下，内容为以换行符(LF)分隔的项目编号，如：

```
1.1
1.2
1.3
1.4
...
```
![img](./img/加固config文件展示.png)

注意：

- 仅需填写对应加固脚本的项目编号即可，不需要填写完整脚本名称。
- 建议只添加未通过检查需要修复的项目编号，已通过检查的项目不需要重复执行加固。
- 默认配置文件(Anolis_security_benchmark_level1.config)内仅加入了需要修复的项目，剔除了系统初始状态已通过的项目。
- Anolis_security_benchmark_level3.config 包含（level-1 + level-3）中需要修复的项目，剔除了系统初始状态已通过的项目。
- level-3 中 SElinux 相关项目（5.2/5.3/5.4）的修复需重启才可生效。目前默认不执行5.4修复项目，仅执行5.3项目。执行后将启用 SElinux 的 Permissive 模式（宽容模式：仅收集日志和警告，不强制拒绝访问），避免造成无法登录系统的情况。如确有需求，请在查看相关日志并确认影响后，手动执行5.4修复脚本并重启系统，即可启用 SElinux 的 Enforcing 模式（工作模式）。

3. 执行脚本：

- 直接执行`run_Anolis_remediation_kit.sh`加固脚本，将默认使用`config`文件夹下`Anolis_security_benchmark_level1.config`配置文件进行加固修复（相当于进行 level-1 等级的加固修复）。
- `-c`参数可指定任意路径下符合要求的 config 文件（要求详见上节），并使用该文件内包含的加固项目，对系统进行加固修复。如`./config/Anolis_security_benchmark_level2.config`。
- 具体命令可参考下文：

```shell
# 默认加固
sh run_Anolis_remediation_kit.sh

# 指定config文件加固修复
sh run_Anolis_remediation_kit.sh -c [configfile]
```

![img](./img/脚本执行-1.png)

![img](./img/脚本执行-2.png)
