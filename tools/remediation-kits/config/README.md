# 1. 部分未通过项目说明：

##### 1.34-ensure-inactive-password-lock-is-30-days-or-less

[benchmark](https://gitee.com/anolis/security-benchmark/blob/master/benchmarks/access-and-control/1.34-ensure-inactive-password-lock-is-30-days-or-less.md)

[修复脚本](https://gitee.com/anolis/security-benchmark/blob/master/remediation-kits/access-and-control/1.34-ensure-inactive-password-lock-is-30-days-or-less.sh)

[检测脚本](https://gitee.com/anolis/security-benchmark/blob/master/scanners/access-and-control/1.34-ensure-inactive-password-lock-is-30-days-or-less.sh)

- 因ISO镜像中，root用户如从未修改过密码，在shadow文件的密码修改时间会记录为空。如此时执行此项加固，将导致root用户被锁定无法正常登陆。
- 鉴于以上原因，此项修复脚本中加入了对密码修改时间的检测，如有密码修改时间为空的账户，将会退出本次修复流程，并展示异常用户。
- 对异常用户进行一次密码修改即可解决该问题，之后可重新执行修复脚本进行修复。
