#!/usr/bin/bash


function helpInfo()
{
# 帮助信息
cat <<HELP
Usage: run_Anolis_Compliance_test.sh [OPTION]... [1-4]...

    -H    display this help and exit.
    -l    Specify detection level(1-4).The default is level-1.
    -c    Customize the configuration file.The default is level-1.
HELP
} 

function scoresShow()
{
# 得分展示
if [[ ! $total -eq 0 ]]; then
    passPercen=`awk 'BEGIN{printf "%.1f%%\n",('$sum_pass'/'$total')*100}'` # pass占比
    failPercen=`awk 'BEGIN{printf "%.1f%%\n",('$sum_fail'/'$total')*100}'` # fail占比
    # score=`awk 'BEGIN{printf "%.0f\n",('$sum_pass'/'$total')*100}'` # 总分
    score=`awk 'BEGIN{printf "%.1f\n",('$sum_pass'/'$total')*100}'` # 总分
    echo -e "Level：$levelName"
    echo -e "类型\t得分\t占比"
    echo -e "pass：\t$sum_pass\t$passPercen"
    echo -e "fail：\t$sum_fail\t$failPercen"
    echo -e "总检查项目：\t$total"
    echo -e "本次得分：\t$score"
else
    echo "There was no result in this test."
fi
}

function assignPath()
{
# 根据编号判断扫描脚本路径
    Dir=""
    if [[ `echo $line | cut -d "." -f1` == "1" ]] ; then
        Dir="$current_path/../../scanners/access-and-control/"
    elif [[ `echo $line | cut -d "." -f1` == "2" ]] ; then
        Dir="$current_path/../../scanners/logging-and-auditing/"
    elif [[ `echo $line | cut -d "." -f1` == "3" ]] ; then
        Dir="$current_path/../../scanners/services/"
    elif [[ `echo $line | cut -d "." -f1` == "4" ]] ; then
        Dir="$current_path/../../scanners/system-configurations/"
    elif [[ `echo $line | cut -d "." -f1` == "5" ]] ; then
        Dir="$current_path/../../scanners/mandatory-access-control/"
    fi
}

function executeScripts()
{
# 执行扫描脚本
    for line in `cat $config`; do
        assignPath
        if [[ ! -z "$Dir" ]] ; then
            cd $Dir
            filename=$(ls | grep -P "^$line\-.*.sh$") # 获取扫描脚本完整名称
            if [[ -a $filename ]] ; then
                res1=$(bash $filename | grep -P "^(pass|fail)$") # 获取扫描脚本执行结果（pass or fail）
                ((total++)) # 总检查量
                if [[ $res1 == "pass" ]]; then
                    ((sum_pass++)) # pass量
                elif [[ $res1 == "fail" ]]; then
                    ((sum_fail++)) # fail量
                fi
                echo -e "$filename\t$res1" >> $current_path/log/.tempfile.log # 记录到临时日志
            fi
            cd $current_path
        else
            echo "$line path error!"
        fi
    done
    [ -f $current_path/log/.tempfile.log ] && cat $current_path/log/.tempfile.log | column -t >> $current_path/log/$logfile # 整理临时日志格式，并记录到正式日志内
    [ -f $current_path/log/$logfile ] && cat $current_path/log/$logfile # 展示本次扫描结果
}

function option() {
# 参数判断，确定config文件
    if [[ -z $1 ]] ; then
        echo "The current level: level-1"
        levelName="level-1"
        config="$current_path/config/Anolis_security_benchmark_level1.config"
    elif [[ $# -eq 2 && $1 == "-l" ]] ; then # -l参数 指定level
        if [[ $2 -eq 1 ]] 2>/dev/null; then
            echo "The current level: level-1"
            levelName="level-1"
            config="$current_path/config/Anolis_security_benchmark_level1.config"
        elif [[ $2 -eq 2 ]] 2>/dev/null; then
            echo "The current level: level-2"
            levelName="level-2"
            config="$current_path/config/Anolis_security_benchmark_level2.config"
        elif [[ $2 -eq 3 ]] 2>/dev/null; then
            echo "The current level: level-3"
            levelName="level-3"
            config="$current_path/config/Anolis_security_benchmark_level3.config"
        elif [[ $2 -eq 4 ]] 2>/dev/null; then
            echo "The current level: level-4"
            levelName="level-4"
            config="$current_path/config/Anolis_security_benchmark_level4.config"
        else
            echo "option \"$2\" error."
        fi
    elif [[ $# -eq 2 && $1 == "-c" ]] ; then # -c参数 指定自定义config
        if [[ -f $2 ]] ; then # 判断config文件是否存在
            echo "configfile is: $2"
            levelName="configfile is: $2"
            config=$2
        else
            echo "$2 is not the correct configuration file."
        fi
    else
        helpInfo
    fi
}

function main() {
    [ ! -e "$current_path/log" ] && mkdir $current_path/log # 创建log目录
    logfile=scanners`date +%Y_%m_%d_%N`.log # 生成本次日志名称
    cat /dev/null > $current_path/log/.tempfile.log # 清空临时日志内容
    total=0 # 初始化总量
    sum_fail=0 # 初始化fail量
    sum_pass=0 # 初始化pass量

    executeScripts
    scoresShow # 得分展示
    scoresShow >> $current_path/log/$logfile # 分数计入日志
    [ -f $current_path/log/$logfile ] && echo "Log is saved in: $current_path/log/$logfile" # 展示日志文件名及路径
}

# systemVersion=false
# cat /etc/redhat-release | grep -Pq "^Anolis\s+OS\s+release\s+8.*" && systemVersion=true # 判断系统版本是否符合执行要求
current_path="$(cd $(dirname $0);pwd)" # 获取绝对路径


option $@
[ ! -z $config ] && main
