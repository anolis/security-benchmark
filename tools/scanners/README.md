# 1、简介
1. 此工具适用于 Anolis OS 操作系统。

2. 自动调用“龙蜥最佳安全基线”中扫描脚本，对系统进行安全合规扫描，并生成扫描结果及日志。

3. 可使用预设配置或按自身需求自由选择扫描项目


# 2、文件及目录说明

- Anolis_security_benchmark_level1.config -- 配置文件，用于存储待扫描的项目编号

- Reference_DengBaoThree.config -- 此 config 文件包含的编号为：已发布的 benchmark 中参考了 等保2.0三级 标准的规则

- run_Anolis_scanners.sh -- 可执行文件，用于调用扫描脚本对系统进行安全合规扫描

- config（目录） -- 用于存放config文件

- log（目录） -- 保存每次执行扫描脚本后的日志文件


# 3、使用方法

## 3.1 确认系统环境

此工具适用于 Anolis OS 操作系统

```shell
# cat /etc/redhat-release
Anolis OS release ...
```

![系统版本](./img/系统版本.png)

- 此工具已在 Anolis OS 8 、 Anolis OS 23 下进行了完整测试。
- 在 v1.3.0 版本更新中，已移除了系统版本检测，目前此工具可在所有Linux发行版下执行。
- 在非 Anolis OS 系统执行此工具，其扫描结果仅供参考。

## 3.2 确认扫描项目

默认使用config文件夹下的`Anolis_security_benchmark_level1.config`中的扫描项目

也可根据使用场景，自行选择`security-benchmark/scanners`目录下的扫描项目

## 3.3 执行自动化工具

1. 自动化工具路径：`security-benchmark/tools/scanners`

2. 自定义config文件：
config文件建议存放在`security-benchmark/tools/scanners/config`目录下，内容为以换行符(LF)分隔的项目编号，如：

```
1.1
1.2
1.3
1.4
...
```
![img](./img/扫描config文件展示.png)

注意：

- 仅需填写对应扫描脚本的项目编号即可，不需要填写完整脚本名称。
- 默认配置文件(Anolis_security_benchmark_level1.config)内加入了已发布的benchmark中安全等级为level-1且有扫描脚本的所有扫描项目。
- 高等级的config文件内，默认包含了低等级的项目。如：level-2 = (level-1 + level-2)、level-3 = (level-1 + level-2 + level-3)、level-4 = (level-1 + level-2 + level-3 + level-4)。

3. 执行脚本：

- 直接执行`run_Anolis_scanners.sh`扫描脚本，将默认使用`config`文件夹下`Anolis_security_benchmark_level1.config`配置文件进行合规扫描（相当于进行 level-1 等级的合规扫描）。
- `-l`参数可指定“龙蜥最佳安全基线”中 level-1 至 level-4 中任意一个等级，并使用该等级的加固要求，对系统进行合规扫描。
- `-c`参数可指定任意路径下符合要求的 config 文件（要求详见上节），并使用该文件内包含的加固项目，对系统进行合规扫描。如`./config/Anolis_security_benchmark_level2.config`。
- 具体命令可参考下文：

```shell
# 默认扫描
sh run_Anolis_scanners.sh

# 指定安全等级扫描
sh run_Anolis_scanners.sh -l [1-4]

# 指定config文件扫描
sh run_Anolis_scanners.sh -c [configfile]
```

![img](./img/脚本执行-1.png)

![img](./img/脚本执行-2.png)
