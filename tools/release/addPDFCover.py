#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
from PyPDF2 import PdfFileReader, PdfFileWriter

def addPDFCover(cover, body, merged):
    output = PdfFileWriter()
    
    # add cover to output
    output.addPage(PdfFileReader(open(cover, "rb")).getPage(0))

    # add body to output
    bodyFile = PdfFileReader(open(body, "rb"))
    pageCount = bodyFile.getNumPages()
    for iPage in range(pageCount):
        output.addPage(bodyFile.getPage(iPage))

    # merge cover and body
    with open(merged, "wb") as outputfile:
        output.write(outputfile)

if __name__ == '__main__':

    if len(sys.argv) != 4:
        print("Usage: ./addPDFCover [cover_file] [body_file] [output_file]")
        exit()

    cover, body, output = sys.argv[1:4]
    addPDFCover(cover, body, output)
