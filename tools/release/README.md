# 1、文件

## 1.1 py文件、sh文件

- input_parm.py # 设置参数形式
- createyml.py # 根据benchmarks结构生成yml文件，默认所有项状态为enabled
- convertmdtopdf.py # md转pdf
- config_zh_font.sh # 配置中文字体

## 1.2 latex文件

- template.latex # 多个markdown文件转pdf的latex模板
- singletemplate.latex # 单个markdown文件转pdf的latex模板

## 1.3 字体文件

放在winfonts目录下，包括：
- simhei.ttf、STSong.ttf、TIMES.TTF、TIMESBD.TTF
        
# 2、环境

## 2.1 适用于Anolis 8

【1】python3，并安装第三方库：`pip3 install PyYAML==5.4.1` 以及`pip3 install PyPDF2==2.12.1`

![image-20230328152252322](./example_images/安装第三方库.png)

【2】依赖pandoc，`yum install -y pandoc`

![image-20230328152601576](./example_images/安装pandoc.png)

【3】依赖Texlive，安装详见[Texlive_install.md](https://gitee.com/anolis/security-benchmark/blob/master/tools/release/Texlive_install.md)

【4】配置字体. `source config_zh_font.sh`

![image-20230328152837247](./example_images/配置字体.png)

## 2.1 适用于Windows 10

【1】python3（测试环境为3.7），并安装第三方库：PyYAML和PyPDF2

【2】依赖pandoc，请提前安装 （2.10.1）

【3】依赖Texlive，安装详见[Texlive_install.md](https://gitee.com/anolis/security-benchmark/blob/master/tools/release/Texlive_install.md)

# 3、运行

## 3.1 生成yml

批量处理时需要生成yml，单个md文件则不需要    

根据benchmarks结构生成yml文件，默认所有项的status为enabled

生成yml文件时，可以加-r指定生成的yml文件名称，不指定则使用默认名称

生成的yml文件存放于tools/release/yml下，

- 使用默认名称：

![使用默认yml名称](./example_images/使用默认yml名称.png)

- 指定名称：

![指定yml名称](./example_images/指定yml名称.png)

## 3.2 md转pdf

可以加-o，指定输出的pdf文件名称

结果放在tools/release/pdf


[1] 单个md

- 使用默认输出pdf文件名称：

![使用默认输出pdf文件名称](./example_images/单个md使用默认输出pdf文件名称.png)

- 指定输出pdf文件名称：

![指定输出pdf文件名称](./example_images/单个指定pdf文件名称.png)

[2] 批量

- 使用默认yml和默认输出文件名称：

![使用默认yml和默认输出文件名称](./example_images/批量时使用默认yml和默认输出文件名称.png)   

- 使用指定yml和默认输出文件名：

![使用指定yml和默认输出文件名](./example_images/使用指定yml和默认输出文件名.png) 

- 使用指定yml文件和指定输出文件名称

![使用指定yml文件和指定输出文件名称](./example_images/使用指定yml文件和指定输出文件名称.png)

- 使用默认yml和指定输出pdf文件名称：

![使用默认yml和指定输出pdf文件名称](./example_images/使用默认yml和指定输出pdf文件名称.png)


## 3.3 为pdf添加封面

使用`addPDFCover.py`脚本为转换后的pdf添加对应的封面`cover.pdf`，具体命令如下（以Anolis 8为例）：

```shell
./addPDFCover.py cover.pdf pdf/converesult.pdf pdf/Anolis_OS_Server_8_Security_Best_Practices_v1.0.0.pdf
```

- 代码执行

![img](./example_images/添加封面-代码执行.png)

- 最终效果

![img](./example_images/最终效果.png)
