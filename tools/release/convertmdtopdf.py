#!/usr/bin/env python3
# -- coding: utf-8 --

import shutil
import sys
import os
import re
from pathlib import Path
import yaml
from input_parm import run_input_parm, loggerr


def start_convert():
    """
    判断-d参数
    """
    loger.info("curdir >>> {}".format(curdir))
    loger.info('abscurdir >>> {}'.format(abscurdir))
    loger.info("")
    loger.info("argpath >>> {}".format(argpath))
    loger.info("absargpath >>> {}".format(absargpath))
    pdf_path = create_pdf_path()
    if absargpath.is_dir():
        loger.info("{} is dir".format(absargpath))
        loger.info("操作 {} 目录下的md文件".format(absargpath))
        merge_dirmd_portal(pdf_path)
    elif absargpath.is_file():
        args_list = sys.argv
        if Path(sys.argv[0]).name == "convertmdtopdf.py":
            if "-r" in args_list:
                raise Exception("参数错误，-d参数给的是文件时，不需要加-r参数")
        outfile = pdf_path.joinpath(args.output_filename)
        loger.info("{} is file".format(absargpath))
        loger.info("start: {} 转为pdf file".format(absargpath))
        single_md_topdf(inputfile=absargpath, outputfile=outfile, filetype="singlemd")
    else:
        loger.error("-d Error : {} is not a directory or a file or does not exist,please check ...".format(absargpath))
        raise Exception(
            "-d Error : {} is not a directory or a file or does not exist, please check ...".format(absargpath))


def create_pdf_path():
    pdf_path = abscurdir.joinpath("pdf")
    if pdf_path.exists():
        # shutil.rmtree(pdf_path)
        # pdf_path.mkdir(parents=True, exist_ok=True)
        pass
    else:
        pdf_path.mkdir(parents=True, exist_ok=True)
    return pdf_path


def merge_dirmd_portal(pdf_path):
    """
    -d 参数是目录时，执行此函数
    作用：处理参数目录下的md文件
    """
    benchmark = list(absargpath.iterdir())
    benchmark.sort()
    outpdfname = args.output_filename
    # result_pdf_name = absargpath.joinpath(outpdfname)
    result_pdf_name = pdf_path.joinpath(outpdfname)
    # merge_mdfile_name = absargpath.joinpath("merge.md")  # 中间文件
    merge_mdfile_name = pdf_path.joinpath("merge.md")  # 中间文件
    buid_merge_mdfile(benchmark, merge_mdfile_name)
    single_md_topdf(inputfile=merge_mdfile_name, outputfile=result_pdf_name)
    del_middle_file(merge_mdfile_name)


def buid_merge_mdfile(benchmark, merge_mdfile_name):
    """
    大类md合成一个md
    """
    parse_yml()
    loger.info("aimlist {}".format(aimlist))
    loger.info("aimlist lenth >>>{}".format(len(aimlist)))
    loger.info("notlist {}".format(notlist))
    loger.info("notlist lenth >>>{}".format(len(notlist)))
    covertemplate()
    handle_subdir(benchmark)

    if merge_mdfile_name.exists():
        merge_mdfile_name.unlink()
    with open(merge_mdfile_name, 'w', encoding='utf-8') as f:
        sub_mdname_list.sort()
        for item in sub_mdname_list:
            with open(item, 'r', encoding='utf-8') as ft:
                read_obj = ft.read()
                f.write(read_obj)


def covertemplate():
    """
    生成批量处理md时用到的latex文件:covertemplate.latex，这个是中间文件，最后会删除
    从yml中获取title和version
    """
    if not ymlpath.exists():
        raise Exception("yml file error: 没有yml文件：{}".format(ymlpath))

    create_covertemplate()


def create_covertemplate():
    if covertmpath.exists():
        covertmpath.unlink()
        readctm()
    else:
        readctm()


def readctm():
    """
    使用template.latex重新生成covertemplate.latex
    """
    with open(covertmpath, 'w', encoding='utf-8') as f:
        with open(tmpath, 'r', encoding="utf-8") as fc:
            anbmobj = fc.readlines()
            for line in anbmobj:
                write_ctm(line, f)


def write_ctm(line, f):
    f.write(line)


def handle_subdir(benchmark):
    """
    处理benchmarks下的子目录，将每个子目录下的md合成一个对应的md文件
    """
    for sec in benchmark:
        if Path(sec).is_file():
            continue
        elif Path(sec).is_dir():
            build_sub_mdfile(sec)
        else:
            raise Exception("既不是目录也不是文件：{}".format(sec))


def build_sub_mdfile(sec):
    """
    benchmarks下的每个类别分别处理
    """
    if sec.name == "access-and-control":
        dir_sort_mdlist = sort_mdir_file(sec)
        judge_mdir_type(sec, 1, dir_sort_mdlist)
    elif sec.name == "logging-and-auditing":
        dir_sort_mdlist = sort_mdir_file(sec)
        judge_mdir_type(sec, 2, dir_sort_mdlist)
    elif sec.name == "services":
        dir_sort_mdlist = sort_mdir_file(sec)
        judge_mdir_type(sec, 3, dir_sort_mdlist)
    elif sec.name == "system-configurations":
        dir_sort_mdlist = sort_mdir_file(sec)
        judge_mdir_type(sec, 4, dir_sort_mdlist)
    elif sec.name == "mandatory-access-control":
        dir_sort_mdlist = sort_mdir_file(sec)
        judge_mdir_type(sec, 5, dir_sort_mdlist)
    else:
        if sec.is_dir():
            raise Exception("意料之外的目录：{}，需要代码适配".format(sec))
        else:
            raise Exception("不存在：{}".format(sec))


def sort_mdir_file(sec):
    """
    子目录下的md排序
    """
    for parentdir, subdirlist, files in os.walk(sec):
        if not subdirlist:
            sort_list = sorted(files, key=lambda mf: int(mf.split('-', 1)[0].split('.')[1]))
            return parentdir, sort_list
        else:
            raise Exception("{} 下有子目录：{},代码不适用".format(sec, subdirlist))


def judge_mdir_type(sec, chapter, dir_sort_mdlist):
    pdf_path = create_pdf_path()
    # submdndme = sec.joinpath(sec.parent, "{}-{}.md".format(chapter, sec.name))
    submdndme = sec.joinpath(pdf_path, "{}-{}.md".format(chapter, sec.name))
    sub_mdname_list.append(str(submdndme))
    if submdndme.is_file() and submdndme.exists():
        submdndme.unlink()
        create_mdfile(submdndme, chapter, dir_sort_mdlist)
    else:
        create_mdfile(submdndme, chapter, dir_sort_mdlist)


def create_mdfile(submdndme, chapter, dir_sort_mdlist):
    """
    根据子目录的不同，创建对应的md文件，用于合并对应字母录下的所有md文件
    """
    with open(submdndme, "w", encoding="utf-8") as f:
        chapter_name = Path(dir_sort_mdlist[0]).name
        f.write("# {} {}\n".format(chapter, chapter_name))
        f.write("\n")
        for itemd in dir_sort_mdlist[1]:
            if itemd.rsplit('.', 1)[0] in notlist:
                continue
            filenamepath = Path(dir_sort_mdlist[0]).joinpath(itemd)
            with open(filenamepath, 'r', encoding='utf-8') as fd:
                linelist = fd.readlines()
                write_content(f, linelist)


def parse_yml():
    """
    解析yml
    """
    if not ymlpath.exists():
        raise Exception("yml file error: 没有yml文件：{}".format(ymlpath))
    with open(ymlpath, encoding='utf-8') as fy:
        yml_dict = yaml.safe_load(fy)
        benchmarklist = yml_dict["benchmarks"]
        loger.info("benchmarklist >>> {}".format(benchmarklist))
        loger.info("benchmarklist lenth >>> {}".format(len(benchmarklist)))
        for mditem in benchmarklist:
            if mditem["status"] == "enabled":
                aim_id_s_list.append(mditem)
            elif mditem["status"] == "disabled":
                not_id_s_list.append(mditem)
            else:
                raise Exception("status error: {} ,必须为enabled或者disabled".format(mditem["status"]))
    aim_not_md_name(aim_id_s_list, "aim")
    aim_not_md_name(not_id_s_list, "not")


def aim_not_md_name(id_s_list, atype):
    """
    判断yml中每个id的status，放入不同的列表中
    enabled，放入aimlist
    disabled，放入notlist
    """
    for md in id_s_list:
        if atype == "aim":
            aimlist.append(md['id'])
        elif atype == "not":
            notlist.append(md['id'])


def write_content(f, linelist):
    """
    所有md逐个逐行写入一个新的的md中
    """
    c = 1
    # --降标题等级
    for line in linelist:
        if c < len(linelist):
            re_match(f, line)
        else:
            re_match(f, line)
            f.write('\n')
            f.write('\\pagebreak\n')
            f.write('\n')
        c += 1
    # --降标题等级


def re_match(f, line):
    if line.startswith("#"):
        if re.match(r"^[#] [a-zA-Z]+", line):
            f.write(line)
        else:
            f.write("#{}".format(line))
    else:
        f.write(line)


def del_middle_file(merge_mdfile_name):
    """
    删除中间文件
    """
    if merge_mdfile_name.exists():
        merge_mdfile_name.unlink()
        loger.info("del middle file: {}".format(merge_mdfile_name))
    if covertmpath.exists():
        covertmpath.unlink()
        loger.info("del midlle file: {}".format(covertmpath))

    for item in sub_mdname_list:
        item_path_obj = Path(item)
        if item_path_obj.exists():
            item_path_obj.unlink()


def single_md_topdf(inputfile=None, outputfile=None, filetype=None):
    """
    执行转换
    """
    singletemplate = abscurdir.joinpath("singletemplate.latex")
    if not tmpath.exists():
        raise Exception("file error: {} 不存在,请检查".format(tmpath))
    if not singletemplate.exists():
        raise Exception("file error: {} 不存在,请检查".format(singletemplate))
    if filetype is None:
        if inputfile is not None:
            os.system("pandoc {} -o {} -H head.tex --highlight-style zenburn -V colorlinks -V urlcolor=NavyBlue --template={} --pdf-engine=xelatex".format(inputfile, outputfile, covertmpath))
    else:
        if filetype == "singlemd":
            os.system(
                "pandoc {} -o {} -H head.tex --highlight-style zenburn -V colorlinks -V urlcolor=NavyBlue --template={} --pdf-engine=xelatex".format(inputfile, outputfile, singletemplate))


if __name__ == "__main__":
    sys.path.append(os.getcwd())
    aim_id_s_list = []
    not_id_s_list = []
    aimlist = []
    notlist = []
    sub_mdname_list = []
    loger = loggerr("convertpdf")
    rel = run_input_parm()
    args = rel[0]
    curdir = rel[1]
    abscurdir = rel[2]
    argpath = rel[3]
    absargpath = rel[4]
    ymlpath = rel[5]
    tmpath = abscurdir.joinpath("template.latex")
    covertmpath = abscurdir.joinpath("covertemplate.latex")

    start_convert()
