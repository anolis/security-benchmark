#!/usr/bin/bash

SElINUX_TYPE=`grep -Ei "^\s*SELINUX=(enforcing|permissive)" /etc/selinux/config`


if  [[ `getenforce` =~ "isabled" ]] || [[ $SELINUX_TYPE =~ "isabled" ]];then
	echo "fail"
else
	echo "pass"
fi
