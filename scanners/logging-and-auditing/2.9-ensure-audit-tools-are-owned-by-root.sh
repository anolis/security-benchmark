#!/usr/bin/bash

result=0
for i in $(stat -c "%U" /sbin/auditctl /sbin/aureport /sbin/ausearch /sbin/autrace /sbin/auditd /sbin/augenrules)
do
    echo $i | grep -Eq '^root$' && ((result++))
done

[[ $result -eq 6 ]] && echo 'pass' || echo 'fail'