#!/usr/bin/bash

fwDenied=$(firewall-cmd --get-log-denied 2>&1)
fwDeniedFile=$(grep -Pm1 "^\s*LogDenied=all\s*$" /etc/firewalld/firewalld.conf | grep -Po "LogDenied=all")

if [[ $fwDenied == "all" && $fwDeniedFile == "LogDenied=all" ]]; then
    echo 'pass'
else
    echo 'fail'
fi
