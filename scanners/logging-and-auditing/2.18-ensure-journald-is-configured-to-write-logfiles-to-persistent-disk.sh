#!/usr/bin/bash

result=false

if [[ -e /etc/systemd/journald.conf ]]; then
    grep -P "^Storage\=persistent$" /etc/systemd/journald.conf && result=true
    if [[ $result == "true" ]]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "fail"
fi