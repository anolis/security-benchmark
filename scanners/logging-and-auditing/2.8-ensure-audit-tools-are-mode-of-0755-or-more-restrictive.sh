#!/usr/bin/bash

result=0
for i in $(stat -c "%a" /sbin/auditctl /sbin/aureport /sbin/ausearch /sbin/autrace /sbin/auditd /sbin/augenrules)
do
    echo $i | grep -Eq '^[0-7][0,1,4,5][0,1,4,5]\s*$' && ((result++))
done

[[ $result -eq 6 ]] && echo 'pass' || echo 'fail'