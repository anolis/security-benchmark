#!/usr/bin/bash

sudoLogFilePath=$(grep -r logfile /etc/sudoers* | sed -e 's/.*logfile=//;s/,? .*//' -e 's/"//g' -e 's|/|\\/|g')

sudoLogRunning=$(auditctl -l | awk "/^ *-w/ &&/"${sudoLogFilePath}"/ &&/ +-p *wa/ &&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)")
sudoLogdisk=$(awk "/^ *-w/ &&/"${sudoLogFilePath}"/ &&/ +-p *wa/ &&(/ key= *[!-~]* *$/||/ -k *[!-~]* *$/)" /etc/audit/rules.d/*.rules /etc/audit/*.rules)
([[ -n "${sudoLogFilePath}" ]] && [[ -n "${sudoLogRunning}" ]] && [[ -n "${sudoLogdisk}" ]] && echo 'pass') || echo 'fail'