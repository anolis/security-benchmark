#!/usr/bin/bash

result=true

for PARTITION in $(findmnt -n -l -k -it $(awk '/nodev/ { print $2 }' /proc/filesystems | paste -sd,) | grep -Pv "noexec|nosuid" | awk '{print $1}'); do
    for PRIVILEGED in $(find "${PARTITION}" -xdev -perm /6000 -type f); do
        [[ $result == true ]] && grep -qsr "${PRIVILEGED}" /etc/audit/rules.d || result=false
    done
done

[[ $result == true ]] && RUNNING=$(auditctl -l)
[ -n "${RUNNING}" ] && for PARTITION in $(findmnt -n -l -k -it $(awk '/nodev/ { print $2 }' /proc/filesystems | paste -sd,) | grep -Pv "noexec|nosuid" | awk '{print $1}'); do
    for PRIVILEGED in $(find "${PARTITION}" -xdev -perm /6000 -type f); do
        printf -- "${RUNNING}" | grep -qs "${PRIVILEGED}" || result=false
    done
done \
|| result=false

[[ $result == true ]] && echo 'pass' || echo 'fail'