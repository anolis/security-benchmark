#!/usr/bin/bash

result=true

for BIT in b32 b64 ; do
    checkRule="^(?=^\s*-a\s+always,exit)(?=.*-F\s+arch=$BIT)(?=.*chmod)(?=.*fchmod)(?=.*chown)(?=.*fchown)(?=.*lchown)(?=.*setxattr)(?=.*lsetxattr)(?=.*fsetxattr)(?=.*removexattr)(?=.*lremovexattr)(?=.*fremovexattr)(?=.*fchownat)(?=.*fchmodat)"
    grep -Pq $checkRule /etc/audit/rules.d/*.rules /etc/audit/*.rules && auditctl -l | grep -Pq $checkRule
    [[ $? -ne 0 ]] && result=false
done

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi
