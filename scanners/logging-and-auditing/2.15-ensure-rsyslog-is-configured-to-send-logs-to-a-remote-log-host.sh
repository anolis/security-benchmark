#!/usr/bin/bash

result=false

grep -Psq "^\*\.\*\s*\@{1,2}.*" /etc/rsyslog.conf /etc/rsyslog.d/*.conf && result=true

if [[ $result == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi
