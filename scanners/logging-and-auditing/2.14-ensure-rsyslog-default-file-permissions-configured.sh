#!/usr/bin/bash

value=$(grep -P "^\s*\\\$FileCreateMode\s+[0-9]{4}\s*$" /etc/rsyslog.conf /etc/rsyslog.d/*.conf | grep -o [0-9]*)

if [[ -n $value ]]; then
    for i in $value ; do
        if echo $i | grep -vsq [0][0-6][0-4][0] ; then
            echo 'fail'
            exit 1
        fi
    done
else
    echo 'fail'
    exit 1
fi

echo 'pass'