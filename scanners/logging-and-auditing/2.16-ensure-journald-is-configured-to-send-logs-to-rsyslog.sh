#!/usr/bin/bash

result=false

if [[ -e /etc/systemd/journald.conf ]]; then
    grep -Pq "^\s*ForwardToSyslog\=yes$" /etc/systemd/journald.conf && result=true
    if [[ $result == "true" ]]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "fail"
fi