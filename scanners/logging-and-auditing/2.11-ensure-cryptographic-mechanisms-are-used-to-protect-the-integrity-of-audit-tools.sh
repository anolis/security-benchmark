#!/usr/bin/bash

if [[ -e /etc/aide/aide.conf ]]; then
    checkContent="p\+i\+n\+u\+g\+s\+b\+acl\+xattrs\+sha512\b$"
    lineNumber=$(grep -Ecs -e "^/sbin/auditctl\s+$checkContent" \
    -e "^/sbin/auditd\s+$checkContent" \
    -e "^/sbin/ausearch\s+$checkContent" \
    -e "^/sbin/aureport\s+$checkContent" \
    -e "^/sbin/autrace\s+$checkContent" \
    -e "^/sbin/augenrules\s+$checkContent" \
    /etc/aide/aide.conf)
    [[ -n $lineNumber && $lineNumber -eq 6 ]] && echo 'pass' || echo 'fail'
else
    echo 'fail'
    exit 1
fi
