#!/usr/bin/bash

if rpm -q audit >/dev/null 2>&1 && rpm -q audit-libs >/dev/null 2>&1 ; then
    result=$(systemctl is-enabled auditd)
    if [[ $result == "enabled" ]]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "fail"
fi
