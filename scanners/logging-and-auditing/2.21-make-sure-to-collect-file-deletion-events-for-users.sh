#!/usr/bin/bash


result='true'

check_rule() {
    if [[ $1 == "x86" ]]; then
        checkRule="^(?=^\s*-a\s+always,exit)(?=.*-F\s+arch=$2)(?=.*rename)(?=.*unlink)(?=.*unlinkat)(?=.*renameat)(?=.*-F\s+auid>=1000)(?=.*-F\s+auid!=-1)"
    elif [[ $1 == "arm" ]]; then
        checkRule="^(?=^\s*-a\s+always,exit)(?=.*-F\s+arch=$2)(?=.*unlinkat)(?=.*renameat)(?=.*-F\s+auid>=1000)(?=.*-F\s+auid!=-1)"
    fi
    check_audit $checkRule
}

check_audit() {
    grep -Psq "$1" /etc/audit/rules.d/*.rules /etc/audit/*.rules && auditctl -l | grep -Pq "$1"
    if [[ $? -ne 0 ]]; then
        result='false'
    fi
}

if [[ $(getconf LONG_BIT) -eq 64 && $(arch) =~ ^x86 ]]; then
    for BIT in "b64" "b32"; do
        check_rule "x86" "$BIT"
    done
elif [[ $(getconf LONG_BIT) -eq 32 && $(arch) =~ ^x86 ]]; then
    BIT='b32'
    check_rule "x86" "$BIT"
elif [[ $(getconf LONG_BIT) -eq 64 && $(arch) =~ ^aarch ]]; then
    for BIT in "b64" "b32"; do
        check_rule "arm" "$BIT"
    done
elif [[ $(getconf LONG_BIT) -eq 32 && $(arch) =~ ^aarch ]]; then
    BIT='b32'
    check_rule "arm" "$BIT"
else
    echo 'fail'
    exit 1
fi

[[ $result == "true" ]] && echo 'pass' || echo 'fail'
