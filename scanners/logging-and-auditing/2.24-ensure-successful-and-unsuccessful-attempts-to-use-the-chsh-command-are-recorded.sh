#!/usr/bin/bash

result=false
checkRule="^(?=^\s*-a\s+always,exit)(?=.*-S\s+all)(?=.*-F\s+path=/usr/bin/chsh)(?=.*-F\s+perm=x)(?=.*-F\s+auid>=1000)(?=.*-F\s+auid!=-1)"

auditctl -l | grep -Pq "$checkRule" && grep -Pq "$checkRule" /etc/audit/rules.d/*.rules /etc/audit/*.rules && result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi
