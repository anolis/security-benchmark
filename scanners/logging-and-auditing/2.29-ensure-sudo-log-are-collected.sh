#!/usr/bin/bash

( awk '/Defaults\s+logfile\s*/ {line = $0; nr = NR} END {if (nr) print line}' /etc/sudoers | grep -Psq "^\s*Defaults\s+logfile\s*=\s*(/?)([a-zA-Z0-9_.-]+/?)*" && echo 'pass' ) || echo 'fail'
