#!/usr/bin/bash

 
[[ -e /etc/audit/auditd.conf ]] && output=$(grep -P "^max_log_file_action\s*=.*" /etc/audit/auditd.conf | cut -f2 -d= | sed -e 's/^[ ]*//g')

if [[ "$output" == "keep_logs" ]] ; then
    echo "pass"
else
    echo "fail"
fi