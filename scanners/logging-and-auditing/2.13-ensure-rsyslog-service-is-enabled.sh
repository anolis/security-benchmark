#!/usr/bin/bash

if rpm -q rsyslog >/dev/null 2>&1 ; then
    result=$(systemctl is-enabled rsyslog)
    if [[ $result == "enabled" ]]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "fail"
fi