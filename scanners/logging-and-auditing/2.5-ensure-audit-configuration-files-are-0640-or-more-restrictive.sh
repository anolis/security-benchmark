#!/usr/bin/bash

result=0

for p in `find /etc/audit/rules.d/ -name *.rules ; find /etc/audit/rules.d/ -name *.conf ; find /etc/audit/audit*.rules ; find /etc/audit/audit*.conf` ; do
    [[ -f $p ]] && file_path=$file_path" ${p}"
done

if [[ -n $file_path ]] ; then
    for access in `stat -c "%a" $file_path`; do
        result=0
        echo $access | grep -Pq '[0-6][0-4][0]' && result=1
        [[ $result == 0 ]] && echo 'fail' && break
    done
else
    echo 'fail'
fi

[[ $result == 1 ]] && echo 'pass'
