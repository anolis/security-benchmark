#!/usr/bin/bash

result=false

grep -q "\-w /etc/sudoers -p wa -k scope
-w /etc/sudoers.d -p wa -k scope" /etc/audit/rules.d/*.rules /etc/audit/*.rules && auditctl -l | grep -Pq "^\-w\s+\/etc\/sudoers\s+\-p\s+wa\s+\-k\s+scope" && auditctl -l | grep -Pq "^\-w\s+\/etc\/sudoers.d\s+\-p\s+wa\s+\-k\s+scope" && result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi