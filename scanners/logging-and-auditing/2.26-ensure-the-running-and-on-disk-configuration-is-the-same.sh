#!/usr/bin/bash

checkResult=$(augenrules --check)

echo $checkResult | grep -Psiq "\bNo\s+change$" && echo 'pass' || echo 'fail'