#!/usr/bin/bash

result=true

checkFile=$(echo "/etc/group" "/etc/passwd" "/etc/gshadow" "/etc/shadow " "/etc/security/opasswd")

for f in $checkFile ; do
    checkRule="^\s*-w\s+${f}\s+-p\s+wa\s+-k\s+.*$"
    grep -Pq $checkRule /etc/audit/rules.d/*.rules /etc/audit/*.rules && auditctl -l | grep -Pq $checkRule
    [[ $? -ne 0 ]] && result=false
done

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi
