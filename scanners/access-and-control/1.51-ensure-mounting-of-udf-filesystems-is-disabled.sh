#!/usr/bin/bash

result=false

modprobe -n -v udf | grep -Pq "^install\s+\/bin/false\b" && test -z "$(lsmod | grep -P "^udf\b")" && grep -Pq "^blacklist\s*udf\b" /etc/modprobe.d/* && result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi
