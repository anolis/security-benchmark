#!/usr/bin/bash

result=false

protocol_value=$(grep -Pim1 "^Protocol\s+" /etc/ssh/sshd_config | awk '{print $2}')

[[ $protocol_value -eq 2 ]] && result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi