#!/usr/bin/bash

loadedSystemConfig=$(sshd -T | grep -Pi "^loglevel\s+(INFO|VERBOSE)$")
configFileSettings=$(grep -Pi '^\s*loglevel\s+' /etc/ssh/sshd_config | grep -Pvi '^\s*loglevel\s+(INFO|VERBOSE)\b')

[[ -z $loadedSystemConfig ]] && echo 'fail' && exit 1
[[ -z $configFileSettings && -n $loadedSystemConfig ]] && echo 'pass' || echo 'fail'
