#!/usr/bin/bash

loadedSystemConfig=$(sshd -T | grep -Pi "^ignorerhosts\s+yes$")
configFileSettings=$(grep -Pim1 '^\s*ignorerhosts\b' /etc/ssh/sshd_config | grep -Pvi '^\s*ignorerhosts\s+yes\b')

[[ -z $loadedSystemConfig ]] && echo 'fail' && exit 1
[[ -z $configFileSettings && -n $loadedSystemConfig ]] && echo 'pass' || echo 'fail'
