#!/usr/bin/bash

result=false

modprobe -n -v cramfs | grep -Pq "^install\s+\/bin/false\b" && test -z "$(lsmod | grep -P "^cramfs\b")" && grep -Pq "^blacklist\b\s*cramfs\b" /etc/modprobe.d/* && result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi