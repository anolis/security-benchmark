#!/usr/bin/bash

result=false

grep "^root:" /etc/passwd | cut -f4 -d: | grep -q 0 && result=true

if [[ $result == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi