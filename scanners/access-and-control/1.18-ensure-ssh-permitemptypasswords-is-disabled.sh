#!/usr/bin/bash

loadedSystemConfig=$(sshd -T | grep -Pi "^permitemptypasswords\s+no$")
configFileSettings=$(grep -Pim1 '^\s*permitemptypasswords\s+' /etc/ssh/sshd_config | grep -Pvi '^\s*permitemptypasswords\s+no\b')

[[ -z $loadedSystemConfig ]] && echo 'fail' && exit 1
[[ -z $configFileSettings && -n $loadedSystemConfig ]] && echo 'pass' || echo 'fail'
