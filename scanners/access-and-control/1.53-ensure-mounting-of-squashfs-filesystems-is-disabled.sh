#!/usr/bin/bash

result=false
echo $(modprobe -n -v squashfs) | grep -Psq "^install\s+\/bin/false\b" && test -z "$(lsmod | grep -P "^squashfs\b")" && grep -Pq "^blacklist\s+squashfs\b" /etc/modprobe.d/* && result=true
if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi