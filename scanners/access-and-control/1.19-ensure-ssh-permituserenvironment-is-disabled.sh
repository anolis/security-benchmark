#!/usr/bin/bash

loadedSystemConfig=$(sshd -T | grep -Pi "^permituserenvironment\s+no$")
configFileSettings=$(grep -Pim1 '^\s*permituserenvironment\s+' /etc/ssh/sshd_config | grep -Pvi '^\s*permituserenvironment\s+no\b')

[[ -z $loadedSystemConfig ]] && echo 'fail' && exit 1
[[ -z $configFileSettings && -n $loadedSystemConfig ]] && echo 'pass' || echo 'fail'
