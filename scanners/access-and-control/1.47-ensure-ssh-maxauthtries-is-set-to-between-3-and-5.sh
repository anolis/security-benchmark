#!/usr/bin/bash

loadedSystemConfig=$(sshd -T | grep -Pi '^\s*maxauthtries\b' | awk '{ if ($2 > 5 || $2 < 3) print 1; else print 0 }')
configFileSettings=$(grep -Pim1 '^\s*maxauthtries\s+' /etc/ssh/sshd_config | awk '{ if ($2 > 5 || $2 < 3) print 1; else print 0 }')

[[ $loadedSystemConfig -ne 0 ]] && echo 'fail' && exit 1
[[ $configFileSettings -eq 0 && $loadedSystemConfig -eq 0 ]] && echo 'pass' || echo 'fail'
