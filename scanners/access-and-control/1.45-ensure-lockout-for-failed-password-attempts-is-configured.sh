#!/usr/bin/bash

grep -Pi "^\s*auth\s+required\s+pam_faillock.so\b\s+.*" /etc/pam.d/password-auth | grep -Pqiv "(?=.*\bdeny=[0-9]+\b)(?=.*unlock_time=[0-9]+)" && echo 'fail' && exit 1
grep -Pi "^\s*auth\s+required\s+pam_faillock.so\b\s+.*" /etc/pam.d/system-auth | grep -Pqiv "(?=.*\bdeny=[0-9]+\b)(?=.*unlock_time=[0-9]+)" && echo 'fail' && exit 1

denyValuePassword=$(grep -Poi "^\s*auth\s+required\s+pam_faillock.so\s+.*\bdeny=[0-9]+\b" /etc/pam.d/password-auth | grep -Pio "\bdeny=[0-9]+\b" | awk -F= '{print $2}')
denyValueSystem=$(grep -Poi "^\s*auth\s+required\s+pam_faillock.so\s+.*\bdeny=[0-9]+\b" /etc/pam.d/system-auth | grep -Pio "\bdeny=[0-9]+\b" | awk -F= '{print $2}')
unlockValuePassword=$(grep -Poi "^\s*auth\s+required\s+pam_faillock.so\s+.*\bunlock_time=[0-9]+\b" /etc/pam.d/password-auth | grep -Pio "\bunlock_time=[0-9]+\b" | awk -F= '{print $2}')
unlockValueSystem=$(grep -Poi "^\s*auth\s+required\s+pam_faillock.so\s+.*\bunlock_time=[0-9]+\b" /etc/pam.d/system-auth | grep -Pio "\bunlock_time=[0-9]+\b" | awk -F= '{print $2}')

if [[ -n $denyValuePassword && -n $denyValueSystem ]] ; then
    for i in $denyValuePassword $denyValueSystem; do
        if [[ $i -lt 3 ]] || [[ $i -gt 8 ]]; then
            echo 'fail'
            exit 1
        fi
    done
else
    echo 'fail'
    exit 1
fi


if [[ -n $unlockValuePassword && -n $unlockValueSystem ]] ; then
    for i in $unlockValuePassword $unlockValueSystem; do
        if [[ $i -lt 600 ]] || [[ $i -gt 1800 ]]; then
            echo 'fail'
            exit 1
        fi
    done
else
    echo 'fail'
    exit 1
fi

echo 'pass'
