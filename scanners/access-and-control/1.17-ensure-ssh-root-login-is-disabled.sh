#!/usr/bin/bash

loadedSystemConfig=$(sshd -T | grep -Pi "^permitrootlogin\s+no$")
configFileSettings=$(grep -Pim1 '^\s*permitrootlogin\s+' /etc/ssh/sshd_config | grep -Pvi '^\s*permitrootlogin\s+no\b')

[[ -z $loadedSystemConfig ]] && echo 'fail' && exit 1
[[ -z $configFileSettings && -n $loadedSystemConfig ]] && echo 'pass' || echo 'fail'
