#!/usr/bin/bash

result_cron_deny=false
result_at_deny=false
result_cron_allow=false
result_at_allow=false

[[ -e /etc/cron.deny ]] || result_cron_deny=true
[[ -e /etc/at.deny ]] || result_at_deny=true

if [[ -e /etc/cron.allow ]] ; then
    stat -c "%a-%U-%G" /etc/cron.allow | grep -Pq '^[0-6][0][0]\-root\-root$' && result_cron_allow=true
fi

if [[ -e /etc/at.allow ]] ; then
    stat -c "%a-%U-%G" /etc/at.allow | grep -Pq '^[0-6][0][0]\-root\-root$' && result_at_allow=true
fi

if [[ "$result_cron_deny" == "true" && "$result_at_deny" == "true" && "$result_cron_allow" == "true" && "$result_at_allow" == "true" ]] ; then
    echo "pass"
else
    echo "fail"
fi
