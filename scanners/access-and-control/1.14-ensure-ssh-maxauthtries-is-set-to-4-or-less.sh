#!/usr/bin/bash

loadedSystemConfig=$(sshd -T | grep -Pi '^\s*maxauthtries\s+[0-4]$')
configFileSettings=$(grep -Pim1 '^\s*maxauthtries\s+' /etc/ssh/sshd_config | grep -Pvi '^\s*maxauthtries\s+[0-4]{1}\b')

[[ -z $loadedSystemConfig ]] && echo 'fail' && exit 1
[[ -z $configFileSettings && -n $loadedSystemConfig ]] && echo 'pass' || echo 'fail'
