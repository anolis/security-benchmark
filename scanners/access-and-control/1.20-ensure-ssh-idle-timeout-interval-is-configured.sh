#!/usr/bin/bash

checkPoint=false
loadedSystemConfig_clientalivecountmax=$(sshd -T | grep -Pi '^clientalivecountmax\s+[0]{1}$')
configFileSettings_clientalivecountmax=$(grep -Pim1 '^\s*ClientAliveCountMax\s+' /etc/ssh/sshd_config | grep -Pvi '^\s*ClientAliveCountMax\s+[0]{1}\b')

[[ -z $loadedSystemConfig_clientalivecountmax ]] && echo 'fail' && exit 1
if [[ -z $configFileSettings_clientalivecountmax && -n $loadedSystemConfig_clientalivecountmax ]]; then
    checkPoint=true
else
    echo 'fail'
    exit 1
fi

[[ -z $configFileSettings_clientalivecountmax && -n $loadedSystemConfig_clientalivecountmax ]] && checkPoint=true || { echo 'fail'; exit 1; }

loadedSystemConfig_clientaliveinterval=$(sshd -T | grep -Pi "^clientaliveinterval\b" | awk '{ if ($2 > 900) print 1; else print 0 }')
configFileSettings_clientaliveinterval=$(grep -Pim1 '^\s*clientaliveinterval\s+' /etc/ssh/sshd_config | awk '{ if ($2 > 900) print 1; else print 0 }')

[[ $loadedSystemConfig_clientaliveinterval -ne 0 ]] && echo 'fail' && exit 1
[[ $configFileSettings_clientaliveinterval -eq 0 && $loadedSystemConfig_clientaliveinterval -eq 0 && $checkPoint == "true" ]] && echo 'pass' || echo 'fail'
