#!/usr/bin/bash

result=false

find /etc/ssh -xdev -type f -name 'ssh_host_*_key.pub' -exec stat -c %G-%U-%a {} \; | grep -Piq "root\-root\-([7][5-7][5-7]|[0-7][5-7][5-7])" || result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi
