#!/usr/bin/bash

export LANG="en_US.UTF-8"
shutdownCheck=$(grep -P "^shutdown\b" /etc/shadow)
haltCheck=$(grep -P "^halt\b" /etc/shadow)
result=false

if [[ $shutdownCheck ]]; then
    passwd -S shutdown | grep -Pq "^shutdown\s+LK"
    result="shutdownpass"
else
    result="shutdownpass"
fi

if [[ $haltCheck ]]; then
    passwd -S halt | grep -Pq "^halt\s+LK"
    result="${result}haltpass"
else
    result="${result}haltpass"
fi

if [[ "$result" == "shutdownpasshaltpass" ]]; then
    echo "pass"
else
    echo "fail"
fi