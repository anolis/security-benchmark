#!/usr/bin/bash

loadedSystemConfig=$(sshd -T | grep -Pi "^banner\s+none$")
configFileSettings=$(grep -Pim1 "^\s*Banner\s+.*$"  /etc/ssh/sshd_config)

if [[ -n $loadedSystemConfig && -n $configFileSettings ]]; then
    echo 'fail' && exit 1
else
    echo 'pass'
fi
