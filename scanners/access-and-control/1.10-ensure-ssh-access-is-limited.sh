#!/usr/bin/bash

result=false

sshd -T | grep -Piq '^(allow|deny)(users|groups)\s+.*' && grep -Piq '^\h*(allow|deny)(users|groups)\h+.*$' /etc/ssh/sshd_config && result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi