#!/usr/bin/bash

loadedSystemConfig=$(sshd -T | grep -Pi "^hostbasedauthentication\s+no$")
configFileSettings=$(grep -Pim1 '^\s*hostbasedauthentication\s+' /etc/ssh/sshd_config | grep -Pvi '^\s*hostbasedauthentication\s+no\b')

[[ -z $loadedSystemConfig ]] && echo 'fail' && exit 1
[[ -z $configFileSettings && -n $loadedSystemConfig ]] && echo 'pass' || echo 'fail'
