#!/usr/bin/bash

export LANG="en_US.UTF-8"
shutdownCheck=$(grep -P "^bin\b" /etc/shadow)
result=false

if [[ $shutdownCheck ]]; then
    passwd -S bin | grep -Pq "\bbin\b\s+LK"
    result="true"
else
    result="true"
fi

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi