#!/usr/bin/bash

result=false

maxstartups_first=$(sshd -T | grep -Pi "^\s*maxstartups\b" | awk '{print $2}' | awk -F: '{print $1}')
maxstartups_second=$(sshd -T | grep -Pi "^\s*maxstartups\b" | awk '{print $2}' | awk -F: '{print $2}')
maxstartups_third=$(sshd -T | grep -Pi "^\s*maxstartups\b" | awk '{print $2}' | awk -F: '{print $3}')

if $(grep -Piq '^\s*maxstartups\s+[0-9]+\:[0-9]+\:[0-9]+\b' /etc/ssh/sshd_config); then
    config_maxstartups_first=$(grep -Pim1 '^\s*maxstartups\b\s+[0-9]+\:[0-9]+\:[0-9]+\b' /etc/ssh/sshd_config | awk '{print $2}' | awk -F: '{print $1}')
    config_maxstartups_second=$(grep -Pim1 '^\s*maxstartups\b\s+[0-9]+\:[0-9]+\:[0-9]+\b' /etc/ssh/sshd_config | awk '{print $2}' | awk -F: '{print $2}')
    config_maxstartups_third=$(grep -Pim1 '^\s*maxstartups\b\s+[0-9]+\:[0-9]+\:[0-9]+\b' /etc/ssh/sshd_config | awk '{print $2}' | awk -F: '{print $3}')
    [[ $maxstartups_first -le 10 ]] && [[ $maxstartups_second -ge 30 ]] && [[ $maxstartups_third -ge $maxstartups_first && $maxstartups_third -le 60 ]] && \
    [[ $config_maxstartups_first -le 10 ]] && [[ $config_maxstartups_second -ge 30 ]] && [[ $config_maxstartups_third -ge $config_maxstartups_first && $config_maxstartups_third -le 60 ]] && \
    result=true
else
    [[ $maxstartups_first -le 10 ]] && [[ $maxstartups_second -ge 30 ]] && \
    # 参数3需大于等于参数1
    [[ $maxstartups_third -ge $maxstartups_first && $maxstartups_third -le 60 ]] && result=true
fi

if [[ "$result" = true ]]; then
    echo "pass"
else
    echo "fail"
fi