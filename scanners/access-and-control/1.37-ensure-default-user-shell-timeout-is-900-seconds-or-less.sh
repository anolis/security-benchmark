#!/usr/bin/bash

result=true

[ -f /etc/bashrc ] && BRC="/etc/bashrc"
for f in "$BRC" /etc/profile /etc/profile.d/*.sh ; do
    val_TMOUT=$(grep -vP "^#.*" $f | grep -Pio "\bTMOUT=[0-9]+\b" | tail -1 | cut -d"=" -f 2)
    [[ -n $val_TMOUT ]] && ex_TMOUT=true
    if [[ -n $val_TMOUT ]] && [[ $val_TMOUT == 0 || $val_TMOUT -gt 900 ]] ; then
        result=false
    else
        :
    fi
done

if [[ $ex_TMOUT == "true" && $result == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi
