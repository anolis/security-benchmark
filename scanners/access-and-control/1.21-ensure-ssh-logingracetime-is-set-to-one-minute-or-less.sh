#!/usr/bin/bash

loadedSystemConfig=$(sshd -T | grep -Pi "^logingracetime\b" | awk '{ if ($2 > 60) print 1; else print 0 }')
configFileType=$(grep -Pim1 '^\s*logingracetime\s+[0-9]+' /etc/ssh/sshd_config | awk '{print $2}' | grep -Poi '[s|m]$' | tr 'A-Z' 'a-z')
configFileSettings=$(grep -Poim1 '^\s*logingracetime\s+[0-9]+' /etc/ssh/sshd_config | awk '{print $2}')

[[ $loadedSystemConfig -ne 0 ]] && echo 'fail' && exit 1

if [[ -z $configFileType && -z $configFileSettings ]]; then
    [[ $loadedSystemConfig -eq 0 ]] && echo 'pass' || echo 'fail'
    exit 0
elif [[ $configFileType == 's' && -n $configFileSettings ]] || [[ -z $configFileType && -n $configFileSettings ]]; then
    [[ $configFileSettings -le 60 ]] && echo 'pass' || echo 'fail'
    exit 0
elif [[ $configFileType == 'm' && -n $configFileSettings ]]; then
    [[ $configFileSettings -le 1 ]] && echo 'pass' || echo 'fail'
    exit 0
else
    echo 'fail'
    exit 0
fi
