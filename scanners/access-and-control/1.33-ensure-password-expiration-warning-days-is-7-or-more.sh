#!/usr/bin/bash

loginPassWarnageVaule=$(grep -P "^\s*PASS_WARN_AGE\s+[0-9]+\b" /etc/login.defs | awk '{ if ($2 >= 7) print $2;}')
userPassWarnageVaule=$(grep -P '^[^:]+:[^!*]' /etc/shadow | awk -F: '$6 == "" || $6 < 7 {print 1}')

if [[ -n $loginPassWarnageVaule && -z $userPassWarnageVaule ]]; then
    echo 'pass'
else
    echo 'fail'
fi
