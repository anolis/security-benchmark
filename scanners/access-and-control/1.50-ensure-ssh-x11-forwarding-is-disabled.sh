#!/usr/bin/bash

loadedSystemConfig=$(sshd -T | grep -Pi "^x11forwarding\s+no$")
configFileSettings=$(grep -Pim1 '^\s*x11forwarding\s+' /etc/ssh/sshd_config | grep -Pvi '^\s*x11forwarding\s+no\b')

[[ -z $loadedSystemConfig ]] && echo 'fail' && exit 1
[[ -z $configFileSettings && -n $loadedSystemConfig ]] && echo 'pass' || echo 'fail'
