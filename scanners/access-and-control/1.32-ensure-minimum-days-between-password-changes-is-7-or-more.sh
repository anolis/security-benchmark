#!/usr/bin/bash

loginPassMinDaysVaule=$(grep -P "^\s*PASS_MIN_DAYS\s+[0-9]+\b" /etc/login.defs | awk '{ if ($2 >= 7) print $2;}')
userPassMinDaysVaule=$(grep -P '^[^:]+:[^!*]' /etc/shadow | awk -F: '$4 == "" || $4 < 7 {print 1}')

if [[ -n $loginPassMinDaysVaule && -z $userPassMinDaysVaule ]]; then
    echo 'pass'
else
    echo 'fail'
fi
