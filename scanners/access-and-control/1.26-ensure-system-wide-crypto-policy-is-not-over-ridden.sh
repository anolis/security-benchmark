#!/usr/bin/bash

result=false

grep -iPq '^\s*CRYPTO_POLICY\s*\=' /etc/sysconfig/sshd || result=true

if [[ $result == true ]]; then
    echo "pass"
else
    echo "fail"
fi