#!/usr/bin/bash

loginPassMaxDaysVaule=$(grep -P "^\s*PASS_MAX_DAYS\s+[0-9]+\b" /etc/login.defs | awk '{ if ($2 <= 365) print $2;}')
userPassMaxDaysVaule=$(grep -P '^[^:]+:[^!*]' /etc/shadow | awk -F: '$5 == "" || $5 > 365 {print 1}')

if [[ -n $loginPassMaxDaysVaule && -z $userPassMaxDaysVaule ]]; then
    echo 'pass'
else
    echo 'fail'
fi
