#!/usr/bin/bash

result=false

grep -Eiq "^\s*password\s+\bsufficient\s+\bpam_unix.so\s+.*\bsha512\s*.*$" /etc/pam.d/password-auth && grep -Eiq "^\s*password\s+\bsufficient\s+\bpam_unix.so\s+.*\bsha512\s*.*$" /etc/pam.d/system-auth && result=true

if [[ $result == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi