#!/usr/bin/bash

grep -Pi "^\s*password\h+sufficient\h+pam_unix.so\h+" /etc/pam.d/system-auth | grep -Pqv "\bremember\s*=\s*[0-9]+" && echo 'fail' && exit 1
grep -Pi "^\s*password\h+requisite\h+pam_pwhistory.so\h+" /etc/pam.d/system-auth | grep -Pqv "\bremember\s*=\s*[0-9]+" && echo 'fail' && exit 1

rememberValue=$(grep -Poi "^\s*password\s+(sufficient|requisite)\s+(pam_unix.so|pam_pwhistory.so)\s+.*\bremember\s*=\s*[0-9]+" /etc/pam.d/system-auth | grep -Pio "\bremember\s*=\s*[0-9]+\b" | awk -F= '{print $2}')

if [[ -n $rememberValue ]] ; then
    for i in $rememberValue; do
        if [[ $i -lt 5 ]] || [[ $i -gt 25 ]] ; then
            echo 'fail'
            exit 1
        fi
    done
else
    echo 'fail'
    exit 1
fi

echo 'pass'
