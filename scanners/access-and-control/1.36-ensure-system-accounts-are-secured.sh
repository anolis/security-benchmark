#!/usr/bin/bash

val_nologin=""
val_lock=""

# /usr/sbin/nologin与/sbin/nologin文件效果一致，配置其中任意一个即可
val_nologin=`awk -F: '($1!="root" && $1!="sync" && $1!="shutdown" && $1!="halt" && $1!~/^\+/ && $3<'"$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)"' && $7!="'"$(which nologin)"'" && $7!="/sbin/nologin" && $7!="/usr/sbin/nologin" && $7!="/bin/false") {print}' /etc/passwd`
val_lock=`awk -F: '($1!="root" && $1!~/^\+/ && $3<'"$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)"') {print $1}' /etc/passwd | xargs -I '{}' passwd -S '{}' | awk '($2!="L" && $2!="LK") {print $1}'`

if [[ $val_nologin == "" && $val_lock == "" ]]; then
    echo "pass"
else
    echo "fail"
fi