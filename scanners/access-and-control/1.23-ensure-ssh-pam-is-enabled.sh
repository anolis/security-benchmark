#!/usr/bin/bash

loadedSystemConfig=$(sshd -T | grep -Pi "^usepam\s+yes$")
configFileSettings=$(grep -Pi '^\s*usepam\s+' /etc/ssh/sshd_config | grep -Pvi '^\s*usepam\s+yes\b')

[[ -z $loadedSystemConfig ]] && echo 'fail' && exit 1
[[ -z $configFileSettings && -n $loadedSystemConfig ]] && echo 'pass' || echo 'fail'
