#!/usr/bin/bash

result=false

stat -c "%a-%U-%G" /etc/ssh/sshd_config | grep -Pq '^[0-7][0][0]\-root\-root$' && result=true

if [[ "$result" == "true" ]] ; then
    echo "pass"
else
    echo "fail"
fi
