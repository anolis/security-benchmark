#!/usr/bin/bash

inactiveVaule=$(grep -P "^\s*INACTIVE=[0-9]+\b" /etc/default/useradd | awk -F= '{ if ($2 <= 30) print $2;}')
userInactiveVaule=$(grep -P '^[^:]+:[^!*]' /etc/shadow | awk -F: '$7 == "" || $7 > 30 { print 1 }')

if [[ -n $inactiveVaule && -z $userInactiveVaule ]]; then
    echo 'pass'
else
    echo 'fail'
fi
