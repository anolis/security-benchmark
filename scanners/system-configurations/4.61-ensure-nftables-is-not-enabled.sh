#!/usr/bin/bash

export LANG="en_US.UTF-8"

result=""

rpm -q nftables | grep -Psq "^nftables\-.*" || result=true
[[ -z "$result" ]] && systemctl is-enabled nftables | grep -Psiq "(disabled|masked)" && result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi