#!/usr/bin/bash

result=""

rpm -qa | grep -Psq "^iptables\-.*" && rpm -qa | grep -Psq "^iptables\-services.*" && result=true
[[ -z "$result" ]] && rpm -q nftables | grep -Psq "^nftables\-.*" && result=true
[[ -z "$result" ]] && rpm -q firewalld | grep -Psq "^firewalld\-.*" && result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi