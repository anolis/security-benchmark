#!/usr/bin/bash

result=false

sysctl kernel.randomize_va_space|grep -Psq "^kernel\.randomize\_va\_space\s+=\s+2$" && [[ -z $(grep -Phs "^kernel\.randomize_va_space\s*=\s*" /run/sysctl.d/*.conf /etc/sysctl.d/*.conf /usr/local/lib/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /lib/sysctl.d/*.conf /etc/sysctl.conf | grep -Psv "^kernel\.randomize_va_space\s*=\s*2\b$") ]] && [[ -n $(grep -Phs "^kernel\.randomize_va_space\s*=\s*" /run/sysctl.d/*.conf /etc/sysctl.d/*.conf /usr/local/lib/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /lib/sysctl.d/*.conf /etc/sysctl.conf | grep -Ps "^kernel\.randomize_va_space\s*=\s*2\b$") ]] && result=true

if [[ "$result" == "true" ]] ; then
    echo "pass"
else
    echo "fail"
fi
