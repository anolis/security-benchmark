#!/usr/bin/bash

result=""

for i in $(cut -d: -f1 /etc/passwd | sort | uniq -d); do
if [[ -n "$i" ]]; then
    result="false"
fi
done

if [[ -z "$result" ]] ; then
    echo "pass"
else
    echo "fail"
fi