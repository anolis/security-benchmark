#!/usr/bin/bash

result=false

sysctl net.ipv4.conf.all.accept_source_route | grep -Psq "^net\.ipv4\.conf\.all\.accept_source_route\s+=\s+0$" && sysctl net.ipv4.conf.default.accept_source_route | grep -Psq "^net\.ipv4\.conf\.default\.accept_source_route\s+=\s+0$" && grep -q "net\.ipv4\.conf\.all\.accept_source_route" /etc/sysctl.conf /etc/sysctl.d/* && grep -q "net\.ipv4\.conf\.default\.accept_source_route" /etc/sysctl.conf /etc/sysctl.d/* && sysctl net.ipv6.conf.all.accept_source_route | grep -Psq "^net\.ipv6\.conf\.all\.accept_source_route\s+=\s+0$" && sysctl net.ipv6.conf.default.accept_source_route | grep -Psq "^net\.ipv6\.conf\.default\.accept_source_route\s+=\s+0$" && grep -q "net\.ipv6\.conf\.all\.accept_source_route" /etc/sysctl.conf /etc/sysctl.d/* && grep -q "net\.ipv6\.conf\.default\.accept_source_route" /etc/sysctl.conf /etc/sysctl.d/* && result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi