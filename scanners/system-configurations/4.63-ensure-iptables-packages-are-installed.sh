#!/usr/bin/bash

result=false

rpm -qa | grep -Psq "^iptables\-.*" && rpm -q iptables-services | grep -Psq "^iptables\-services\-.*" && result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi