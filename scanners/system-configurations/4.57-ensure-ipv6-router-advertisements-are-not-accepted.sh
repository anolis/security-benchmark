#!/usr/bin/bash

result=false

sysctl net.ipv6.conf.all.accept_ra | grep -Psq "^net\.ipv6\.conf\.all.accept_ra\s+=\s+0$" && sysctl net.ipv6.conf.default.accept_ra | grep -Psq "^net\.ipv6\.conf\.default\.accept_ra\s+=\s+0$" && grep -q "net\.ipv6\.conf\.all\.accept_ra" /etc/sysctl.conf /etc/sysctl.d/* && grep -q "net\.ipv6\.conf\.default\.accept_ra" /etc/sysctl.conf /etc/sysctl.d/* && result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi