#!/usr/bin/bash

result=false

sysctl net.ipv4.ip_forward | grep -Psq "^net\.ipv4\.ip\_forward\s+=\s+0$" && sysctl net.ipv6.conf.all.forwarding | grep -Psq "^net\.ipv6\.conf\.all\.forwarding\s+=\s+0$" && result=""
[[ -z "$result" ]] && result=$(grep -Ps "^\s*net\.ipv4\.ip_forward\s*=.*" /etc/sysctl.conf /etc/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /run/sysctl.d/*.conf | grep -Pvs "net.ipv4.ip_forward\s*=\s*0\s*$")
[[ -z "$result" ]] && result=$(grep -Ps "^\s*net\.ipv6\.conf\.all\.forwarding\s*=.*" /etc/sysctl.conf /etc/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /run/sysctl.d/*.conf | grep -Pvs "net.ipv6.conf.all.forwarding\s*=\s*0\s*$")

if [[ -z "$result" ]] ; then
    echo "pass"
else
    echo "fail"
fi