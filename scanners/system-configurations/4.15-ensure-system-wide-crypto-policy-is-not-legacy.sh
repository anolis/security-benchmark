#!/usr/bin/bash

result=false

grep -Eiq '^\s*LEGACY\s*(\s+#.*)?$' /etc/crypto-policies/config || result=true

if [[ "$result" == "true" ]] ; then
    echo "pass"
else
    echo "fail"
fi