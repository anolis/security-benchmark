#!/usr/bin/bash

source /etc/profile
HIST=$(echo $HISTFILESIZE | awk '($1 > 100 || $1 == "" ) {print 1}')
HIST_FILE=$(grep -P "^HISTFILESIZE\b\=[0-9]+\b" /etc/profile | grep -Po "\b[0-9]+\b" | awk '($1 > 100 || $1 == "" ) {print 1}')

[[ -z $HIST && -z $HIST_FILE ]] && echo 'pass' || echo 'fail'