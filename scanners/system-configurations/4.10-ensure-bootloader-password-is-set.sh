#!/usr/bin/bash

result=false

if [[ -e /boot/grub2/user.cfg  ]]; then
    grep -Pqs '^\h*GRUB2_PASSWORD\h*=\h*.+$' /boot/grub2/*.cfg && result=true
    if [[ "$result" == "true" ]] ; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "fail"
fi