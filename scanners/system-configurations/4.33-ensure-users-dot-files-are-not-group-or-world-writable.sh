#!/usr/bin/bash

result=""
user=""
dir=""

for i in $(awk -F: '($1!~/(halt|sync|shutdown)/ && $7!~/^(\/usr)?\/sbin\/nologin(\/)?$/ && $7!~/(\/usr)?\/bin\/false(\/)?$/) {print $1":"$6}' /etc/passwd); do
    user=$(echo "$i" | cut -d: -f1)
    dir=$(echo "$i" | cut -d: -f2)
    if [[ ! -d "$dir" ]]; then
        [[ -z "$result" ]] && result="false"
    else
        for file in "$dir"/.*; do
            if [[ ! -h "$file" ]] && [[ -f "$file" ]]; then
                fileperm=$(stat -L -c "%A" "$file")
                if [[ "$(echo "$fileperm" | cut -c6)" != "-" ]] || [[ "$(echo "$fileperm" | cut -c9)" != "-" ]]; then
                    [[ -z "$result" ]] && result="false"
                fi
            fi
        done
    fi
done

if [[ -z "$result" ]] ; then
    echo "pass"
else
    echo "fail"
fi