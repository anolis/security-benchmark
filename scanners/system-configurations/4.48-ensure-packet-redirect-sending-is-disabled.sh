#!/usr/bin/bash

result=false

sysctl net.ipv4.conf.all.send_redirects | grep -Psq "^net\.ipv4\.conf\.all\.send\_redirects\s+=\s+0$" && sysctl net.ipv4.conf.default.send_redirects | grep -Psq "^net\.ipv4\.conf\.default\.send\_redirects\s+=\s+0$" && grep -Psq "net\.ipv4\.conf\.all\.send_redirects" /etc/sysctl.conf /etc/sysctl.d/* && grep -Psq "net\.ipv4\.conf\.default\.send_redirects" /etc/sysctl.conf /etc/sysctl.d/* && result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi