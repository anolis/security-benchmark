#!/usr/bin/bash

result=false

grep -Pq "^\s*ExecStart=-/usr/lib/systemd/systemd-sulogin-shell(\s+emergency|\s*)\s*(\s+#.*)?$" /usr/lib/systemd/system/emergency.service && grep -Pq "^\s*ExecStart=-/usr/lib/systemd/systemd-sulogin-shell(\s+rescue\s*|\s*)\s*(\s+#.*)?$" /usr/lib/systemd/system/rescue.service && result=true

if [[ "$result" == "true" ]] ; then
    echo "pass"
else
    echo "fail"
fi