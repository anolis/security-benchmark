#!/usr/bin/bash

result=false

lsmod | grep -Pq "^sctp\b" || { modprobe -n -q sctp && modprobe -n -v sctp | grep -Pq "^install\s*\/bin\/true\s*$" && result=true; }

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi
