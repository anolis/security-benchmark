#!/usr/bin/bash

result=false

modprobe -n -vq dccp && result=""
lsmod | grep -q dccp || { [[ -z "$result" ]] && result=true ; }

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi