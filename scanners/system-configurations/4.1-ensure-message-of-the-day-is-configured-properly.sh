#!/usr/bin/bash

result=false

grep -Eiq "(\\\v|\\\r|\\\m|\\\s|$(grep '^ID=' /etc/os-release | cut -d= -f2 | sed -e 's/"//g'))" /etc/motd || result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi