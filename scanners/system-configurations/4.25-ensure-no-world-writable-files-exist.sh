#!/usr/bin/bash

result=`df --local -P | awk '{if (NR!=1) print $6}' | xargs -I '{}' find '{}' -xdev -type f -perm -0002`

if [[ -z "$result" ]] ; then
    echo "pass"
else
    echo "fail"
fi