#!/usr/bin/bash

result=""
user=""
dir=""

for i in $( awk -F: '($1!~/(halt|sync|shutdown|nfsnobody)/ && $7!~/^(\/usr)?\/sbin\/nologin(\/)?$/ && $7!~/(\/usr)?\/bin\/false(\/)?$/) {print $1":"$6}' /etc/passwd); do
    user=$(echo "$i" | cut -d: -f1)
    dir=$(echo "$i" | cut -d: -f2)
    if [[ ! -d "$dir" ]]; then
        [[ -z "$result" ]] && result="false"
    else
        owner="$(stat -L -c "%U" "$dir")"
        if [[ "$owner" != "$user" ]] && [[ "$owner" != "root" ]]; then
            [[ -z "$result" ]] && result="false"
        fi
    fi
done

if [[ -z "$result" ]] ; then
    echo "pass"
else
    echo "fail"
fi