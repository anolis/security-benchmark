#!/usr/bin/bash

result=false

sysctl net.ipv4.conf.all.secure_redirects | grep -Psq "^net\.ipv4\.conf\.all\.secure_redirects\s+=\s+0$" && sysctl net.ipv4.conf.default.secure_redirects | grep -Psq "^net\.ipv4\.conf\.default\.secure_redirects\s+=\s+0$" && grep -q "net\.ipv4\.conf\.all\.secure_redirects" /etc/sysctl.conf /etc/sysctl.d/* && grep -q "net\.ipv4\.conf\.default\.secure_redirects" /etc/sysctl.conf /etc/sysctl.d/* && result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi