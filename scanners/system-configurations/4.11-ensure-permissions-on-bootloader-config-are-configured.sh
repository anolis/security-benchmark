#!/usr/bin/bash

osID=$(cat /etc/os-release | grep -Pi "^ID=" | cut -f2 -d= | sed -rn "s/\"//gp")

[[ -f $(realpath /boot/grub2/grub.cfg) ]] && file_path=$(realpath /boot/grub2/grub.cfg)
[[ -f $(realpath /boot/grub2/grubenv) ]] && file_path=$file_path" $(realpath /boot/grub2/grubenv)"
[[ -f $(realpath /boot/grub2/user.cfg) ]] && file_path=$file_path" $(realpath /boot/grub2/user.cfg)"
[[ -f $(realpath /boot/efi/EFI/$osID/grubenv) ]] && file_path=$file_path" $(realpath /boot/efi/EFI/$osID/grubenv)"
result=0

if [[ -n $file_path ]] ; then
    for access in $file_path; do
        result=0
        stat -c '%U:%G' $access | grep -Pq "^root\:root$" && stat -c '%a' $access | grep -Pq "^[0-7]00$" && result=1
        [[ $result == 0 ]] && echo 'fail' && break
    done
else
    echo 'fail'
fi

[[ $result == 1 ]] && echo 'pass'
