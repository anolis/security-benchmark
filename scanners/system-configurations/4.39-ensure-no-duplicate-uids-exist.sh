#!/usr/bin/bash

result=""

for i in $(cut -f3 -d":" /etc/passwd | sort -n | uniq -d); do
if [[ -n "$i" ]]; then
    result="false"
fi
done

if [[ -z "$result" ]] ; then
    echo "pass"
else
    echo "fail"
fi