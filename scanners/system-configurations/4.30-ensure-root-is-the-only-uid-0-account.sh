#!/usr/bin/bash

result=`awk -F: '($3 == 0) { print $1 }' /etc/passwd`

if [[ "$result" == "root" ]] ; then
    echo "pass"
else
    echo "fail"
fi