#!/usr/bin/bash

export LANG="en_US.UTF-8"

if command -v nmcli >/dev/null 2>&1 ; then
    if nmcli radio all | grep -Eq '\s*\S+\s+disabled\s+\S+\s+disabled\b'; then
        echo "pass"
    else
        echo "fail"
    fi
elif [[ -n "$(find /sys/class/net/*/ -type d -name wireless)" ]]; then
    t=0
    mname=$(for driverdir in $(find /sys/class/net/*/ -type d -name wireless | xargs -0 dirname); do basename "$(readlink -f "$driverdir"/device/driver/module)";done | sort -u)
    for dm in $mname; do
        if grep -Eq "^\s*install\s+$dm\s+/bin/(true|false)" /etc/modprobe.d/*.conf; then
            /bin/true
        else
            t=1
        fi
    done

[[ "$t" -eq 0 ]] && echo "pass"
[[ "$t" -eq 1 ]] && echo "fail"
else
    echo "pass"
fi