#!/usr/bin/bash

result=false

result=$(grep -E -s "^\s*net\.ipv4\.icmp_ignore_bogus_error_responses\s*=\s*[^1]+" /etc/sysctl.conf /etc/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /run/sysctl.d/*.conf)
[[ -z "$result" ]] && grep -Ps "^\s*net\.ipv4\.icmp_ignore_bogus_error_responses\s*=.*" /etc/sysctl.conf /etc/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /run/sysctl.d/*.conf | grep -Pvsq "net.ipv4.icmp_ignore_bogus_error_responses\s*=\s*1\s*$" || result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi
