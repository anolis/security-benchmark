#!/usr/bin/bash


result=true

grep -Eisq '^\s*Enable\s*=\s*true' /etc/gdm/custom.conf && result=false

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi