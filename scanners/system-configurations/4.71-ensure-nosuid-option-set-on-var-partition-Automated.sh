#!/usr/bin/bash

[[ -e /etc/fstab ]] && [[ -n $(grep -Ps "\s+\/var\s+.*nosuid" /etc/fstab) ]] && [[ -n $(findmnt --kernel /var | grep nosuid) ]] && echo "pass" || echo "fail"