#!/usr/bin/bash

result=""

for i in $(cut -s -d: -f4 /etc/passwd | sort -u ); do
    grep -q -P "^.*?:[^:]*:$i:" /etc/group
    if [[ $? -ne 0 ]]; then
        [[ -z "$result" ]] && result="false"
    fi
done

if [[ -z "$result" ]] ; then
    echo "pass"
else
    echo "fail"
fi