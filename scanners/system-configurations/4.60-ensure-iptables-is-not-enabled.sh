#!/usr/bin/bash

export LANG="en_US.UTF-8"

result=""

rpm -q iptables-services | grep -Psq "^iptables\-services.*" || result=true
[[ -z "$result" ]] && systemctl is-enabled iptables | grep -Psiq "(disabled|masked)" && result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi