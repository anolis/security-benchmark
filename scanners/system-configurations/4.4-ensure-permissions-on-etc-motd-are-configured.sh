#!/usr/bin/bash

result=false

if [[ ! -f /etc/motd ]] ; then
    result=true
elif [[ ! -h /etc/motd ]] ; then
    stat -c "%a-%U-%G" /etc/motd | grep -Pq '^[0-6][0-4][0-4]\-root\-root$' && result=true
elif [[ -h /etc/motd ]] && [[ -f /var/lib/update-motd/motd ]] ; then
    stat -c "%a-%U-%G" /var/lib/update-motd/motd | grep -Pq '^[0-6][0-4][0-4]\-root\-root$' && stat -c "%U-%G" /etc/motd | grep -Pq '^root\-root$' && result=true
fi

if [[ "$result" == "true" ]] ; then
    echo "pass"
else
    echo "fail"
fi
