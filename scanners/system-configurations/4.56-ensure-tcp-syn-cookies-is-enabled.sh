#!/usr/bin/bash

result=false

result=`grep -E -r "^\s*net\.ipv4\.tcp_syncookies\s*=\s*[02]" /etc/sysctl.conf /etc/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf`
[[ -z "$result" ]] && sysctl net.ipv4.tcp_syncookies | grep -Psq "^net\.ipv4\.tcp_syncookies\s+=\s+1$" && result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi