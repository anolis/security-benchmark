#!/usr/bin/bash

result=false

rpm -q nftables | grep -Psq "^nftables\-.*" && systemctl is-enabled nftables | grep -Psiq "^enabled$" && result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi