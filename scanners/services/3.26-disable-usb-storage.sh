#!/usr/bin/bash

result=true
echo $(modprobe -n -v usb-storage) | grep -Psq "^install\s+\/bin\/true$" || result=false
lsmod | grep -Pq "^usb(_|-)storage\b" && result=false

if [[ "$result" == "true" ]]; then
  echo "pass"
else
  echo "fail"
fi
