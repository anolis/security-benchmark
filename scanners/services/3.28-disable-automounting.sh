#!/usr/bin/bash

export LANG="en_US.UTF-8"

rpmAutofs=$(rpm -qa | grep ^autofs)
[[ -z $rpmAutofs ]] || sysAutofs=$(systemctl is-enabled autofs)

if [[ -z $rpmAutofs ]]; then
    echo 'pass'
elif [[ $sysAutofs == 'disabled' ]]; then
    echo 'pass'
else
    echo 'fail'
fi