#!/usr/bin/bash

if [[ "$(rpm -qa vsftpd)" ]]; then
    result=$(systemctl is-enabled vsftpd)
    if [[ $result != enabled ]]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi