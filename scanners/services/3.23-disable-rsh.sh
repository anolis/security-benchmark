#!/usr/bin/bash

if [[ "$(rpm -qa rsh-server)" ]]; then
    result=$(systemctl is-enabled rsh.socket)
    if [[ $result != enabled ]]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi