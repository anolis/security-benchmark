#!/usr/bin/bash

if [[ "$(rpm -qa ypserv)" ]]; then
    result=$(systemctl is-enabled ypserv)
    if [[ $result != enabled ]]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi