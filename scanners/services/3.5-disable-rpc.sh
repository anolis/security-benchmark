#!/usr/bin/bash

if [[ "$(rpm -qa rpcbind)" ]]; then
    result=$(systemctl is-enabled rpcbind)
    if [[ $result != enabled ]]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi