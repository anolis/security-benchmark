#!/usr/bin/bash

export LANG="en_US.UTF-8"
result=false

rpm -q kexec-tools | grep -Psiq "^package\s+kexec-tools\s+is\s+not\s+installed$" && result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi
