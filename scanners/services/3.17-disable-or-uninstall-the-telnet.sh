#!/usr/bin/bash

if [[ "$(rpm -qa | grep telnet-server)" ]]; then
    result=$(systemctl is-enabled telnet.socket)
    if [[ $result != enabled ]]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi
