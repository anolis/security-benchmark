#!/usr/bin/bash

if [[ "$(rpm -qa nfs-utils)" ]]; then
    result=$(systemctl is-enabled nfs-server)
    if [[ $result != enabled ]]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi