#!/usr/bin/bash

result=false

rpm -q chrony | grep -Psiq "^chrony\-" && result=true

if [[ "$result" == "true" ]]; then
    echo "pass"
else
    echo "fail"
fi
