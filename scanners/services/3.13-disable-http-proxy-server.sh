#!/usr/bin/bash

if [[ "$(rpm -qa squid)" ]]; then
    result=$(systemctl is-enabled squid)
    if [[ $result != enabled ]]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi