#!/usr/bin/bash

if [[ "$(rpm -qa postfix)" ]]; then
    result=$(systemctl is-enabled postfix.service)
    if [[ $result != enabled ]]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi
