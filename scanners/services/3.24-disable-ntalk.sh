#!/usr/bin/bash

if [[ "$(rpm -qa ntalk)" ]]; then
    result=$(systemctl is-enabled ntalk.socket)
    if [[ $result != enabled ]]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi