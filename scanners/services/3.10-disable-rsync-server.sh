#!/usr/bin/bash

if [[ "$(rpm -qa rsync-daemon)" ]]; then
    result=$(systemctl is-enabled rsyncd)
    if [[ $result != enabled ]]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi