龙蜥社区security-benchmark Adopters

- OpenSCAP
  - Anolis OS 8 及其最佳安全基线已经完成与[OpenSCAP国际知名社区](https://github.com/ComplianceAsCode/content)的映射与适配，并被OpenSCAP社区合入，详见 [OpenSCAP社区Anolis OS 8 standard.profile](https://github.com/ComplianceAsCode/content/blob/master/products/anolis8/profiles/standard.profile)。
  - Anolis OS 23 及其最佳安全基线已经完成与[OpenSCAP国际知名社区](https://github.com/ComplianceAsCode/content)的映射与适配，并被OpenSCAP社区合入，详见 [OpenSCAP社区Anolis OS 23 standard.profile](https://github.com/ComplianceAsCode/content/blob/master/products/anolis23/profiles/standard.profile)。