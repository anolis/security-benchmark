# openscap 使用指南

## 背景及简介
- Anolis OS 安全最佳实践基线是由龙蜥社区系统安全委员会SIG，联合阿里云、统信软件等SIG成员，基于最新Anolis OS发行版联合发布的。该最佳实践基于 Anolis OS 下游发行版 Alibaba Cloud Linux 3 和 UOS Server v20 大规模产品落地的安全经验和实践进行打造，兼容等保 2.0 安全标准的安全保护基本要求，包括access-and-control、logging-and-auditing、services、 system-configurations、mandatory-access-control等五个方面共150多条内容，以安全基线的形式为社区用户提供配置各种安全功能的指南，帮助企业和组织对安全风险做出更清晰的决策。

- SCAP（Security Content Automation Protocol：安全内容自动化协议）由NIST（National Institute of Standards and Technology：美国国家标准与技术研究院）提出，NIST期望利用SCAP解决三个棘手的问题：一是实现高层政策法规（如FISMA，ISO27000系列）等到底层实施的落地，二是将信息安全所涉及的各个要素标准化（如统一漏洞的命名及严重性度量），三是将复杂的系统配置核查工作自动化。SCAP是当前美国比较成熟的一套信息安全评估标准体系，其标准化、自动化的思想对信息安全行业产生了深远的影响。

- OpenSCAP不仅在Redhat、CentOS等OS的yum源中集成，也在Alibaba Cloud Linux、Anolis OS、欧拉以及统信等国内OS厂商的发行版的yum源中集成，此外OpenSCAP社区也被Wazuh等更大的安全社区集成。

- **通过 OS 产品支持整合，安全策略对接，Anolis OS 及龙蜥安全最佳基线完成在 OpenSCAP 项目上的官方集成，成为 OpenSCAP 支持的国内 OS 发行版和标准安全基线：**

  > - 龙蜥安全最佳基线完成在 OpenSCAP 产品中的映射：将龙蜥安全最佳基线中的一百余条安全加固条目及规则，一对一映射到了 OpenSCAP 产品的标准规则中
  >
  > - 直接使用 OpenSCAP 即可调用龙蜥安全最佳基线对 Anolis OS 操作系统进行合规扫描及安全修复，并生成扫描结果
  >
  > - 下图将展示 OpenSCAP Anolis OS 标准规则中的部分规则与龙蜥安全最佳基线的映射关系：
  >
  > ![image-20230106133437490](./img/image-20230106133437490.png)
  >
  > ……
  >
  > - [OpenSCAP-content社区](https://github.com/ComplianceAsCode/content)
  >
  > - [龙蜥安全最佳基线社区](https://gitee.com/anolis/security-benchmark)

## 功能介绍

- openscap 可在多场景下对不同的操作系统或产品进行合规检查或修复，常用命令及功能如下：

1. `oscap` 命令：可对本地系统进行合规检测或修复
2. `oscap-podman` 命令：可对容器或容器镜像进行合规扫描
3. `oscap-ssh` 命令：可通过 ssh 端口，对远程主机进行合规检测或修复
4. `oscap-chroot` 命令：用于对安装在任意路径上的文件系统进行脱机 SCAP 评估
5. `oscap-remediate-offline` 命令：使用 Systemd 的更新服务为 SCAP 评估或修复
6. `oscap-vm`命令：对虚拟机进行合规检测
7. 可被其他安全产品内置或调用，（如wazuh等）对系统进行定期检测及修复
    ......

- 下文主要介绍 oscap、oscap-podman、oscap-ssh 的使用方法


## 使用方法

### 一、安装 openscap 环境

1. 使用 yum 源进行安装：

```bash
yum install -y scap-security-guide
```

- yum 安装过程：

![yum安装过程](./img/image-20230106103627237.png)

- rpm代码仓库[链接](https://gitee.com/src-anolis-os/scap-security-guide)

### 二、合规扫描及修复

1. 本地合规扫描与修复：
- 通过 oscap 命令，对本地操作系统进行合规扫描或修复，需有对应的规则文件（ssg-anolis8-ds-1.2.xml）。

```bash
# 进入 xml 文件目录
cd /usr/share/xml/scap/ssg/content/
# 查看 anolis8 合规检测规则信息
oscap info ssg-anolis8-ds-1.2.xml
# 可通过 --remediate 参数，控制是否在扫描的同时进行修复操作
oscap  xccdf eval --profile "xccdf_org.ssgproject.content_profile_standard" --results-arf results.xml --report report.html ssg-anolis8-ds-1.2.xml
```

- 扫描过程：

![image.png](./img/1668590954473-68c81106-9ada-4c87-a062-521cd3328b25.png)

- **检测结果展示（report.html）：**

![image.png](./img/1668590862544-815ca687-d464-4b8b-ab7f-5f79ad496beb.png)

> Rule results：展示规则检测结果的汇总信息
>
> Severity of failed rules：失败项目中严重等级占比信息
>
> Score：得分信息

![image-20230106104103318](./img/image-20230106104103318.png)

![image-20230106104115829](./img/image-20230106104115829.png)

> 显示了各检测项目的结果，点击可查看详细信息

2. oscap-ssh 远程合规检测
- 通过 oscap-ssh 命令，对远程主机的操作系统进行合规扫描或修复。
- 依赖条件：
    1. 两端均需安装依赖包（openscap-utils、openscap-scanner）
    2. 本地端需有对应的规则文件（ssg-anolis8-ds-1.2.xml），可本地编译或从其他机器上拷贝

```bash
# 安装依赖环境
yum install -y openscap-utils openscap-scanner
# 在本地端执行，**.**.**.** 22 为远端主机ip地址及SSH端口号。
oscap-ssh root@**.**.**.** 22 xccdf eval --profile "xccdf_org.ssgproject.content_profile_standard" --report remote_report.html ssg-anolis8-ds-1.2.xml
```

- 首次链接提示：

![image.png](./img/1668591168557-678aef87-364b-442e-b91f-acc13ff9956c.png)

*输入 yes，然后正确填写对端用户密码*

* 先将本地的 ssg-anolis8-ds-1.2.xml 文件传输给远端主机：

![image.png](./img/1668591213718-38afdeb2-4001-411e-a080-9a8d967f0e10.png)

- 检查过程：

*ssg-anolis8-ds-1.2.xml 文件传输成功后，会自动开始合规检查*：

![image.png](./img/1668591266221-1dedd99e-8cd5-4173-8bb6-a7d29966bff1.png)

* 检查结果回传：

![image.png](./img/1668591392045-f4d56643-276e-4aae-a37b-b2528bb63e55.png)

> **扫描过程中，本地端会将规则文件（ssg-anolis8-ds-1.2.xml）通过网络传输至远端主机，并执行扫描。扫描完成后，扫描结果也会通过网络回传至本地端。**

3. 容器与镜像 合规检测
- 通过 oscap-podman 命令，可对容器或容器镜像进行合规扫描。

```bash
# openscap 目前仅支持 podman 命令检查，docker 暂无法使用
yum install -y podman
# 拉取 镜像
podman pull openanolis/anolisos
podman images
podman run docker.io/openanolis/anolisos:latest
podman start ff8a2e6ac8f5
podman ps -a
```

![image.png](./img/1668591724004-7f793817-a065-4bdd-9532-81e263ac0e24.png)

* 合规检查：

```bash
# 可分别对镜像和容器进行检查，通过 ID 进行指定和区分
oscap-podman [IMAGE ID] oval eval --report vulnerability.html ssg-anolis8-oval.xml
oscap-podman [CONTAINER ID] oval eval --report vulnerability.html ssg-anolis8-oval.xml
```

![image.png](./img/1668591778973-b590c90d-0433-49bb-8dc4-9ef444e299d9.png)

* 查看检查结果：

![image-20230106105605782](./img/image-20230106105605782.png)

> 左上角展示了本次扫描的结果汇总
>
> 下方展示了具体扫描项目以及扫描结果

4. Anolis 8 上 Wazuh v4.1.5 openscap 监控功能

- 编译与启动服务

```bash
yum install -y git make gcc gcc-c++ vim
yum install -y cmake
yum install -y libstdc++-static
yum install make cmake gcc gcc-c++ python3 python3-policycoreutils automake autoconf libtool
yum install jq -y
ln -s /usr/bin/python3.6 /usr/bin/python
wget https://github.com/wazuh/wazuh/archive/v4.1.5.tar.gz
tar -zxvf v4.1.5.tar.gz
cd wazuh-4.1.5/
./install.sh
# 启动wazuh服务
/var/ossec/bin/ossec-control start
```

编译过程：

![img](./img/1641043278037-a65e1e2b-e59e-45f4-85eb-d859e357af50.png)

![img](./img/1641043293469-0c92ceb2-e831-4424-a757-64c35b385adb.png)

![img](./img/1641043310755-e9c5205e-c1f8-44ee-a932-68e6e61a4806.png)

![img](./img/1641043325593-ada157f4-a39f-4650-9e2f-b09ab640133f.png)

![image.png](./img/1668592764187-7a5ba1f7-6d29-4c54-a7e7-75dea40441c8.png)

- 配置/var/ossec/etc/ossec.conf
  - timeout：超时时间
  - interval：检测间隔时间
  - scan-on-start：是否开机扫描


```bash
# vim /var/ossec/etc/ossec.conf +59
    <!-- open scap -->
    <wodle name="open-scap">
    <disabled>no</disabled>
    <timeout>1800</timeout>
    <interval>5min</interval>
    <scan-on-start>yes</scan-on-start>
    <content type="xccdf" path="ssg-anolis8-ds.xml">
      <profile>xccdf_org.ssgproject.content_profile_standard</profile>
    </content>
  </wodle>
```

![image.png](./img/1668593075617-ee1bdd1c-61cf-4bbb-a5c8-5f0f345d995c.png)

- 复制open-scap组件及ds.xml文件到指定位置并重启wazuh服务

```bash
cd wazuh-4.1.5/wodles
cp -r oscap /var/ossec/wodles/
cp ssg-anolis8-ds.xml /var/ossec/wodles/oscap/content/
/var/ossec/bin/ossec-control restart
```

- 查看检测日志

```bash
# 查看扫描日志
cat /var/ossec/logs/ossec.log | grep oscap | tail -n 10

# 查看总分以及每一项的具体的得分情况
cat /var/ossec/logs/alerts/alerts.log | grep oscap | tail -n 10
```

![image.png](./img/1668593244822-eb45b2bc-0ded-446a-9151-468c0b6d29da.png)

![image.png](./img/1668593280811-91fbb199-15d7-403d-999d-1a537a580fbd.png)

图中`score`为当前扫描的得分情况

- 持续监控

![image.png](./img/1668649530629-2f5ded47-f3bd-4528-a964-c1bcee791863.png)

可以看到，按配置文件填写的时间，每5分钟对主机进行一次合规检测
