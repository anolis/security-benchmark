| 基线编号 | 文件名                                                       | 项目名称                                             | 路径                                | level |
| -------- | ------------------------------------------------------------ | ---------------------------------------------------- | ----------------------------------- | ----- |
| 1.1      | 1.1-ensure-cron-daemon-is-enabled.md                         | 1.1 确保 cron 守护进程正常启用                       | benchmarks/access-and-control       | 1     |
| 1.2      | 1.2-ensure-permissions-on-etc-crontab-are-configured.md      | 1.2 确保 /etc/crontab 的权限配置正确                 | benchmarks/access-and-control       | 1     |
| 1.3      | 1.3-ensure-permissions-on-etc-cron.hourly-are-configured.md  | 1.3 确保 /etc/cron.hourly 的权限配置正确             | benchmarks/access-and-control       | 1     |
| 1.4      | 1.4-ensure-permissions-on-etc-cron.daily-are-configured.md   | 1.4 确保 /etc/cron.daily 的权限配置正确              | benchmarks/access-and-control       | 1     |
| 1.5      | 1.5-ensure-permissions-on-etc-cron.weekly-are-configured.md  | 1.5 确保 /etc/cron.weekly 的权限配置正确             | benchmarks/access-and-control       | 1     |
| 1.6      | 1.6-ensure-permissions-on-etc-cron.monthly-are-configured.md | 1.6 确保 /etc/cron.monthly 的权限配置正确            | benchmarks/access-and-control       | 1     |
| 1.7      | 1.7-ensure-permissions-on-etc-cron.d-are-configured.md       | 1.7 确保 /etc/cron.d 的权限配置正确                  | benchmarks/access-and-control       | 1     |
| 1.8      | 1.8-ensure-at-cron-is-restricted-to-authorized-users.md      | 1.8 确保 at/cron 用户权限列表配置正确                | benchmarks/access-and-control       | 1     |
| 1.9      | 1.9-ensure-permissions-on-etc-ssh-sshd_config-are-configured.md | 1.9 确保 /etc/ssh/sshd_config 的权限配置正确         | benchmarks/access-and-control       | 1     |
| 1.10     | 1.10-ensure-ssh-access-is-limited.md                         | 1.10 确保 SSH 的访问权限受到管控                     | benchmarks/access-and-control       | 2     |
| 1.11     | 1.11-ensure-permissions-on-ssh-private-host-key-files-are-configured.md | 1.11 确保 SSH 私钥文件的权限配置正确                 | benchmarks/access-and-control       | 1     |
| 1.12     | 1.12-ensure-permissions-on-ssh-public-host-key-files-are-configured.md | 1.12 确保 SSH 公钥文件的权限配置正确                 | benchmarks/access-and-control       | 1     |
| 1.13     | 1.13-ensure-ssh-loglevel-is-appropriate.md                   | 1.13 确保 SSH 的 LogLevel 配置正确                   | benchmarks/access-and-control       | 1     |
| 1.14     | 1.14-ensure-ssh-maxauthtries-is-set-to-4-or-less.md          | 1.14 确保 SSH 的 MaxAuthTries 设置为 4 或更小        | benchmarks/access-and-control       | 1     |
| 1.15     | 1.15-ensure-ssh-ignorerhosts-is-enabled.md                   | 1.15 确保 SSH IgnoreRhosts 参数正确配置              | benchmarks/access-and-control       | 1     |
| 1.16     | 1.16-ensure-ssh-hostbasedauthentication-is-disabled.md       | 1.16 确保 SSH HostbasedAuthentication 参数正确配置   | benchmarks/access-and-control       | 1     |
| 1.17     | 1.17-ensure-ssh-root-login-is-disabled.md                    | 1.17 确保禁用 root 用户通过 SSH 登录                 | benchmarks/access-and-control       | 1     |
| 1.18     | 1.18-ensure-ssh-permitemptypasswords-is-disabled.md          | 1.18 确保 SSH PermitEmptyPasswords 参数正确配置      | benchmarks/access-and-control       | 1     |
| 1.19     | 1.19-ensure-ssh-permituserenvironment-is-disabled.md         | 1.19 确保 SSH PermitUserEnvironment 参数正确配置     | benchmarks/access-and-control       | 1     |
| 1.20     | 1.20-ensure-ssh-idle-timeout-interval-is-configured.md       | 1.20 确保 SSH 配置了空闲超时时间                     | benchmarks/access-and-control       | 1     |
| 1.21     | 1.21-ensure-ssh-logingracetime-is-set-to-one-minute-or-less.md | 1.21 确保 SSH LoginGraceTime 被设置为 60 秒或更短    | benchmarks/access-and-control       | 1     |
| 1.22     | 1.22-ensure-ssh-warning-banner-is-configured.md              | 1.22 确保 SSH 已配置警告横幅                         | benchmarks/access-and-control       | 1     |
| 1.23     | 1.23-ensure-ssh-pam-is-enabled.md                            | 1.23 确保 SSH PAM 已启用                             | benchmarks/access-and-control       | 1     |
| 1.24     | 1.24-ensure-ssh-maxstartups-is-configured.md                 | 1.24 确保 SSH MaxStartups 参数正确配置               | benchmarks/access-and-control       | 1     |
| 1.25     | 1.25-ensure-ssh-maxsessions-is-set-to-10-or-less.md          | 1.25 确保 SSH MaxSessions 参数设置为 10 或以下       | benchmarks/access-and-control       | 1     |
| 1.26     | 1.26-ensure-system-wide-crypto-policy-is-not-over-ridden.md  | 1.26 确保全局加密策略不被覆盖                        | benchmarks/access-and-control       | 1     |
| 1.27     | 1.27-ensure-password-creation-requirements-are-configured.md | 1.27 确保设置了密码复杂性检查策略                    | benchmarks/access-and-control       | 1     |
| 1.28     | 1.28-ensure-lockout-for-failed-password-attempts-is-configured.md | 1.28 确保配置了密码验证失败超过阈值后锁定用户        | benchmarks/access-and-control       | 1     |
| 1.29     | 1.29-ensure-password-reuse-is-limited.md                     | 1.29 确保正确限制密码复用                            | benchmarks/access-and-control       | 1     |
| 1.30     | 1.30-ensure-password-hashing-algorithm-is-sha-512.md         | 1.30 确保密码哈希算法为 SHA-512                      | benchmarks/access-and-control       | 1     |
| 1.31     | 1.31-ensure-password-expiration-is-365-days-or-less.md       | 1.31 确保密码过期时间不超过365天                     | benchmarks/access-and-control       | 1     |
| 1.32     | 1.32-ensure-minimum-days-between-password-changes-is-7-or-more.md | 1.32 确保修改密码的间隔时间不少于7天                 | benchmarks/access-and-control       | 1     |
| 1.33     | 1.33-ensure-password-expiration-warning-days-is-7-or-more.md | 1.33 确保密码过期警告天数大于等于7天                 | benchmarks/access-and-control       | 1     |
| 1.34     | 1.34-ensure-inactive-password-lock-is-30-days-or-less.md     | 1.34 确保不活跃用户的锁定时间为 30 天或更短          | benchmarks/access-and-control       | 1     |
| 1.35     | 1.35-ensure-all-users-last-password-change-date-is-in-the-past.md | 1.35 确保所有用户最后一次修改密码的时间早于当前时间  | benchmarks/access-and-control       | 2     |
| 1.36     | 1.36-ensure-system-accounts-are-secured.md                   | 1.36 确保虚拟用户不可通过 shell 登录                 | benchmarks/access-and-control       | 1     |
| 1.37     | 1.37-ensure-default-user-shell-timeout-is-900-seconds-or-less.md | 1.37 确保用户shell超时时间小于等于900秒              | benchmarks/access-and-control       | 1     |
| 1.38     | 1.38-ensure-default-group-for-the-root-account-is-gid-0.md   | 1.38 确保 root 帐号的默认组为GID 0                   | benchmarks/access-and-control       | 1     |
| 1.39     | 1.39-ensure-default-user-umask-is-027-or-more-restrictive.md | 1.39 确保 umask 设置为 027 或更严格                  | benchmarks/access-and-control       | 1     |
| 1.40     | 1.40-ensure-access-to-the-su-command-is-restricted.md        | 1.40 确保 su 命令的使用受到限制                      | benchmarks/access-and-control       | 1     |
| 1.41     | 1.41-ensure-ssh-server-use-protocol_2.md                     | 1.41 ssh服务使用协议2                                | benchmarks/access-and-control       | 1     |
| 1.42     | 1.42-ensure-that-the-password-expires-between-30-and-90-days.md | 1.42 确保密码过期时间在 30 至 90 天之间              | benchmarks/access-and-control       | 1     |
| 1.43     | 1.43-ensure-that-the-minimum-password-change-between-7-and-14-days.md | 1.43 确保修改密码的间隔时间在 7 至 14 天之间         | benchmarks/access-and-control       | 1     |
| 1.44     | 1.44-ensure-that-password-reuse-limit-is-between-5-and-25-times.md | 1.44 确保正确限制密码复用                            | benchmarks/access-and-control       | 1     |
| 1.45     | 1.45-ensure-lockout-for-failed-password-attempts-is-configured.md | 1.45 确保正确配置了密码尝试失败次数和失败后锁定时间  | benchmarks/access-and-control       | 1     |
| 1.46     | 1.46-ensure-default-user-shell-timeout-is-between-600-and-1800-seconds.md | 1.46 确保用户 shell 超时时间在600至1800秒之间        | benchmarks/access-and-control       | 1     |
| 1.47     | 1.47-ensure-ssh-maxauthtries-is-set-to-between-3-and-5.md    | 1.47 确保 SSH 的 MaxAuthTries 设置为3~5              | benchmarks/access-and-control       | 1     |
| 1.48     | 1.48-restrict-the-terminals-that-can-be-managed-over-the-network.md | 1.48 对通过网络进行管理的终端进行限制                | benchmarks/access-and-control       | 2     |
| 1.49     | 1.49-lock-or-delete-the-shutdown-and-halt-users.md           | 1.49 锁定或删除shutdown、halt用户                    | benchmarks/access-and-control       | 1     |
| 1.50     | 1.50-ensure-ssh-x11-forwarding-is-disabled.md                | 1.50 确保SSH X11转发功能被禁用                    | benchmarks/access-and-control       | 1     |
| 1.51     | 1.51-ensure-mounting-of-udf-filesystems-is-disabled.md           | 1.51 确保udf文件系统的挂载被禁用                    | benchmarks/access-and-control       | 1     |
| 1.52     | 1.52-ensure-mounting-of-cramfs-filesystems-is-disabled           | 1.52 确保cramfs文件系统的挂载被禁用                    | benchmarks/access-and-control       | 1     |
| 1.53     | 1.53-ensure-mounting-of-squashfs-filesystems-is-disabled           | 1.53 确保squashfs文件系统的挂载被禁用                    | benchmarks/access-and-control       | 1     |
| 1.54     | 1.54-lock-the-bin-and-adm-users.md           | 1.54 锁定bin、adm用户                    | benchmarks/access-and-control       | 1     |
| 2.1      | 2.1-ensure-audit-log-files-are-not-read-or-write-accessible-by-unauthorized-users.md | 2.1 确保审计日志的文件权限被正确配置                 | benchmarks/logging-and-auditing     | 1     |
| 2.2      | 2.2-ensure-only-authorized-users-own-audit-log-files.md      | 2.2 确保审计日志文件的所有者为已授权用户             | benchmarks/logging-and-auditing     | 1     |
| 2.3      | 2.3-ensure-only-authorized-groups-ownership-of-audit-log-files.md | 2.3 确保审计日志文件的所属组为已授权的用户组         | benchmarks/logging-and-auditing     | 1     |
| 2.4      | 2.4-ensure-the-audit-log-directory-is-0750-or-more-restrictive.md | 2.4 确保审计目录的权限小于 0750                      | benchmarks/logging-and-auditing     | 1     |
| 2.5      | 2.5-ensure-audit-configuration-files-are-0640-or-more-restrictive.md | 2.5 确保审计配置文件的权限小于0640                   | benchmarks/logging-and-auditing     | 1     |
| 2.6      | 2.6-ensure-only-authorized-accounts-own-the-audit-configuration-files.md | 2.6 确保审计配置文件的所有者为已授权用户             | benchmarks/logging-and-auditing     | 1     |
| 2.7      | 2.7-ensure-only-authorized-groups-own-the-audit-configuration-files.md | 2.7 确保审计配置文件的所属组为已授权的用户组         | benchmarks/logging-and-auditing     | 1     |
| 2.8      | 2.8-ensure-audit-tools-are-mode-of-0755-or-more-restrictive.md | 2.8 确保审计工具的权限为 0755 或更低                 | benchmarks/logging-and-auditing     | 1     |
| 2.9      | 2.9-ensure-audit-tools-are-owned-by-root.md                  | 2.9 确保审计工具属于root用户                         | benchmarks/logging-and-auditing     | 1     |
| 2.10     | 2.10-ensure-audit-tools-are-group-owned-by-root.md           | 2.10 确保审计工具属于root用户组                      | benchmarks/logging-and-auditing     | 1     |
| 2.11     | 2.11-ensure-cryptographic-mechanisms-are-used-to-protect-the-integrity-of-audit-tools.md | 2.11 确保使用加密机制来保护审计工具的完整性          | benchmarks/logging-and-auditing     | 1     |
| 2.12     | 2.12-ensure-rsyslog-is-installed.md                          | 2.12 确保已安装 rsyslog                              | benchmarks/logging-and-auditing     | 1     |
| 2.13     | 2.13-ensure-rsyslog-service-is-enabled.md                    | 2.13 确保 rsyslog 服务已启用                         | benchmarks/logging-and-auditing     | 1     |
| 2.14     | 2.14-ensure-rsyslog-default-file-permissions-configured.md   | 2.14 确保正确配置了 rsyslog 默认文件权限             | benchmarks/logging-and-auditing     | 1     |
| 2.15     | 2.15-ensure-rsyslog-is-configured-to-send-logs-to-a-remote-log-host.md | 2.15 确保 rsyslog 配置了远程日志主机                 | benchmarks/logging-and-auditing     | 2     |
| 2.16     | 2.16-ensure-journald-is-configured-to-send-logs-to-rsyslog.md | 2.16 确保配置 journald 向 rsyslog 发送日志           | benchmarks/logging-and-auditing     | 1     |
| 2.17     | 2.17-ensure-journald-is-configured-to-compress-large-log-files.md | 2.17 确保 journald 日志压缩功能正确启用              | benchmarks/logging-and-auditing     | 1     |
| 2.18     | 2.18-ensure-journald-is-configured-to-write-logfiles-to-persistent-disk.md | 2.18 确保 journald 日志文件写入硬盘功能正确开启      | benchmarks/logging-and-auditing     | 1     |
| 2.19     | 2.19-ensure-audit-is-installed.md                            | 2.19 确保审计工具已安装                              | benchmarks/logging-and-auditing     | 1     |
| 2.20     | 2.20-ensure-audit-service-is-enabled.md                      | 2.20 确保已启用审计服务                              | benchmarks/logging-and-auditing     | 3     |
| 2.21     | 2.21-make-sure-to-collect-file-deletion-events-for-users.md  | 2.21 确保收集用户的文件删除事件                      | benchmarks/logging-and-auditing     | 3     |
| 2.22     | 2.22-ensure-that-changes-to-the-system-management-scope-sudoers-are-collected.md | 2.22 确保收集对系统管理范围（sudoers）的更改         | benchmarks/logging-and-auditing     | 3     |
| 2.23     | 2.23-ensure-that-events-that-modify-user-group-information-are-collected.md | 2.23 确保收集修改用户/组信息的事件                   | benchmarks/logging-and-auditing     | 3     |
| 2.24     | 2.24-ensure-successful-and-unsuccessful-attempts-to-use-the-chsh-command-are-recorded.md | 2.24 确保记录成功或不成功使用chsh命令          | benchmarks/logging-and-auditing    | 3     |
| 2.25     | 2.25-ensure-audit-logs-are-not-automatically-deleted.md | 2.25 确保审计日志不会自动删除          | benchmarks/logging-and-auditing    | 3     |
| 2.26     | 2.26-ensure-the-running-and-on-disk-configuration-is-the-same.md | 2.26 确保审计系统内存配置信息和磁盘配置信息相同          | benchmarks/logging-and-auditing    | 1     |
| 2.27 | 2.27-ensure-that-the-firewall-logging-function-is-enabled.md | 2.27 确保开启防火墙日志记录功能 | benchmarks/logging-and-auditing | 3 |
| 2.28 | 2.28-ensure-login-and-logout-events-are-collected.md | 2.28 确保收集登录和注销事件 | benchmarks/logging-and-auditing | 3 |
| 2.29 | 2.29-ensure-sudo-log-are-collected.md | 2.29 确保收集sudo日志 | benchmarks/logging-and-auditing | 3 |
| 2.30 | 2.30-ensure-events-that-modify-the-sudo-log-file-are-collected.md | 2.30 确保收集sudo日志的改动记录 | benchmarks/logging-and-auditing | 3 |
| 2.31 | 2.31-ensure-use-of-privileged-commands-are-collected.md | 2.31 确保收集特权命令的使用记录 | benchmarks/logging-and-auditing | 3 |
| 2.32 | 2.32-ensure-discretionary-access-control-permission-modification-events-are-collected.md | 2.32 确保收集访问控制权限修改事件 | benchmarks/logging-and-auditing | 3 |
| 3.1      | 3.1-disable-http-server.md                                   | 3.1 禁用HTTP Server                                  | benchmarks/services                 | 1     |
| 3.2      | 3.2-disable-ftp-server.md                                    | 3.2 禁用FTP Server                                   | benchmarks/services                 | 1     |
| 3.3      | 3.3-disable-dns-server.md                                    | 3.3 禁用DNS Server                                   | benchmarks/services                 | 1     |
| 3.4      | 3.4-disable-nfs.md                                           | 3.4 禁用NFS                                          | benchmarks/services                 | 1     |
| 3.5      | 3.5-disable-rpc.md                                           | 3.5 禁用RPC                                          | benchmarks/services                 | 1     |
| 3.6      | 3.6-disable-ldap-server.md                                   | 3.6 禁用LDAP Server                                  | benchmarks/services                 | 1     |
| 3.7      | 3.7-disable-dhcp-server.md                                   | 3.7 禁用DHCP Server                                  | benchmarks/services                 | 1     |
| 3.8      | 3.8-disable-cups.md                                          | 3.8 禁用CUPS                                         | benchmarks/services                 | 1     |
| 3.9      | 3.9-disable-nis-server.md                                    | 3.9 禁用NIS Server                                   | benchmarks/services                 | 1     |
| 3.10     | 3.10-disable-rsync-server.md                                 | 3.10 禁用Rsync Server                                | benchmarks/services                 | 1     |
| 3.11     | 3.11-disable-avahi-server.md                                 | 3.11 禁用Avahi Server                                | benchmarks/services                 | 1     |
| 3.12     | 3.12-disable-snmp-server.md                                  | 3.12 禁用SNMP Server                                 | benchmarks/services                 | 1     |
| 3.13     | 3.13-disable-http-proxy-server.md                            | 3.13 禁用HTTP Proxy Server                           | benchmarks/services                 | 1     |
| 3.14     | 3.14-disable-samba.md                                        | 3.14 禁用Samba                                       | benchmarks/services                 | 1     |
| 3.15     | 3.15-disable-imap-and-pop3-server.md                         | 3.15 禁用IMAP 和POP3 Server                          | benchmarks/services                 | 1     |
| 3.16     | 3.16-disable-smtp-protocol.md                                | 3.16 禁用使用smtp协议的postfix服务                   | benchmarks/services                 | 1     |
| 3.17     | 3.17-disable-or-uninstall-the-telnet.md                      | 3.17 禁用或卸载telnet                                | benchmarks/services                 | 1     |
| 3.18     | 3.18-uninstall-the-avahi-server.md                           | 3.18 卸载Avahi                                       | benchmarks/services                 | 1     |
| 3.19     | 3.19-uninstall-the-kexec-tools.md                            | 3.19 卸载 kexec-tools                                | benchmarks/services                 | 3     |
| 3.20     | 3.20-uninstall-the-firstboot.md                              | 3.20 卸载 firstboot                                  | benchmarks/services                 | 1     |
| 3.21     | 3.21-uninstall-the-wpa_supplicant.md                         | 3.21 卸载 wpa_supplicant                             | benchmarks/services                 | 1     |
| 3.22     | 3.22-ensure-NIS-Client-is-not-installed.md | 3.22 确保NIS客户端被卸载                          | benchmarks/services                 | 1     |
| 3.23     | 3.23-disable-rsh.md                                          | 3.23 禁用rsh                                         | benchmarks/services                 | 1     |
| 3.24     | 3.24-disable-ntalk.md                                        | 3.24 禁用ntalk                                       | benchmarks/services                 | 1     |
| 3.25     | 3.25-ensure-xinetd-is-not-installed.md                       | 3.25 确保xinetd被卸载                                      | benchmarks/services                 | 1     |
| 3.26     | 3.26-disable-usb-storage.md                                  | 3.26 禁用USB存储                                     | benchmarks/services                 | 1     |
| 3.27     | 3.27-ensure-time-synchronization-is-installed.md                | 3.27 确保时间同步服务已安装                               | benchmarks/services                 | 1     |
| 3.28     | 3.28-disable-automounting.md                | 3.28 禁用自动挂载                               | benchmarks/services                 | 1     |
| 4.1      | 4.1-ensure-message-of-the-day-is-configured-properly.md      | 4.1 确保登录提示消息的内容符合要求                   | benchmarks/system-configurations    | 1     |
| 4.2      | 4.2-ensure-local-login-warning-banner-is-configured-properly.md | 4.2 确保本地登录提示消息的内容符合要求               | benchmarks/system-configurations    | 1     |
| 4.3      | 4.3-ensure-remote-login-warning-banner-is-configured-properly.md | 4.3 确保远程登录提示消息的内容符合要求               | benchmarks/system-configurations    | 1     |
| 4.4      | 4.4-ensure-permissions-on-/etc/motd-are-configured.md        | 4.4 确保 /etc/motd 的权限配置正确                    | benchmarks/system-configurations    | 1     |
| 4.5      | 4.5-ensure-permissions-on-/etc/issue-are-configured.md       | 4.5 确保 /etc/issue 的权限配置正确                   | benchmarks/system-configurations    | 1     |
| 4.6      | 4.6-ensure-permissions-on-/etc/issue.net-are-configured.md   | 4.6 确保 /etc/issue.net 的权限配置正确               | benchmarks/system-configurations    | 1     |
| 4.7      | 4.7-ensure-gpgcheck-is-globally-activated.md                 | 4.7 确保 gpgcheck 全局激活                           | benchmarks/system-configurations    | 1     |
| 4.8      | 4.8-ensure-AIDE-is-installed.md                              | 4.8 确保正确安装 AIDE                                | benchmarks/system-configurations    | 1     |
| 4.9      | 4.9-ensure-filesystem-integrity-is-regularly-checked.md      | 4.9 确保定期检查文件系统完整性                       | benchmarks/system-configurations    | 1     |
| 4.10     | 4.10-ensure-bootloader-password-is-set.md                    | 4.10 确保设置了 bootloader 密码                      | benchmarks/system-configurations    | 2     |
| 4.11     | 4.11-ensure-permissions-on-bootloader-config-are-configured.md | 4.11 确保 bootloader 配置文件的权限配置正确          | benchmarks/system-configurations    | 1     |
| 4.12     | 4.12-ensure-authentication-required-for-single-user-mode.md  | 4.12 确保进入单用户模式需要进行身份验证              | benchmarks/system-configurations    | 1     |
| 4.13     | 4.13-ensure-core-dumps-are-restricted.md                     | 4.13 确保核心转储受到限制                            | benchmarks/system-configurations    | 1     |
| 4.14     | 4.14-ensure-address-space-layout-randomization-(ASLR)-is-enabled.md | 4.14 确保地址空间布局随机化（ASLR）被启用            | benchmarks/system-configurations    | 1     |
| 4.15     | 4.15-ensure-system-wide-crypto-policy-is-not-legacy.md       | 4.15 确保系统全局加密策略符合要求                    | benchmarks/system-configurations    | 1     |
| 4.16     | 4.16-ensure-sticky-bit-is-set-on-all-world-writable-directories.md | 4.16 确保所有全局可写目录都设置了 sticky 位          | benchmarks/system-configurations    | 1     |
| 4.17     | 4.17-ensure-permissions-on-/etc/passwd-are-configured.md     | 4.17 确保 /etc/passwd 文件权限配置正确               | benchmarks/system-configurations    | 1     |
| 4.18     | 4.18-ensure-permissions-on-/etc/shadow-are-configured.md     | 4.18 确保 /etc/shadow 文件权限配置正确               | benchmarks/system-configurations    | 1     |
| 4.19     | 4.19-ensure-permissions-on-/etc/group-are-configured.md      | 4.19 确保 /etc/group 文件权限配置正确                | benchmarks/system-configurations    | 1     |
| 4.20     | 4.20-ensure-permissions-on-/etc/gshadow-are-configured.md    | 4.20 确保 /etc/gshadow 文件权限配置正确              | benchmarks/system-configurations    | 1     |
| 4.21     | 4.21-ensure-permissions-on-/etc/passwd--are-configured.md    | 4.21 确保 /etc/passwd- 文件权限配置正确              | benchmarks/system-configurations    | 1     |
| 4.22     | 4.22-ensure-permissions-on-/etc/shadow--are-configured.md    | 4.22 确保 /etc/shadow- 文件权限配置正确              | benchmarks/system-configurations    | 1     |
| 4.23     | 4.23-ensure-permissions-on-/etc/group--are-configured.md     | 4.23 确保 /etc/group- 文件权限配置正确               | benchmarks/system-configurations    | 1     |
| 4.24     | 4.24-ensure-permissions-on-/etc/gshadow--are-configured.md   | 4.24 确保 /etc/gshadow- 文件权限配置正确             | benchmarks/system-configurations    | 1     |
| 4.25     | 4.25-ensure-no-world-writable-files-exist.md                 | 4.25 确保没有所有人可写的文件                        | benchmarks/system-configurations    | 2     |
| 4.26     | 4.26-ensure-no-unowned-files-or-directories-exist.md         | 4.26 确保所有文件或目录都配置了所有者                | benchmarks/system-configurations    | 2     |
| 4.27     | 4.27-ensure-no-ungrouped-files-or-directories-exist.md       | 4.27 确保所有文件或目录都配置了所属组                | benchmarks/system-configurations    | 2     |
| 4.28     | 4.28-ensure-no-password-fields-are-not-empty.md              | 4.28 确保所有用户的密码不为空                        | benchmarks/system-configurations    | 2     |
| 4.29     | 4.29-ensure-root-path-integrity.md                           | 4.29 确保 /root/bin 目录的权限及所有者符合要求       | benchmarks/system-configurations    | 2     |
| 4.30     | 4.30-ensure-root-is-the-only-uid-0-account.md                | 4.30 确保 UID 为 0 的用户只有 root                   | benchmarks/system-configurations    | 2     |
| 4.31     | 4.31-ensure-users-home-directories-permissions-are-750-or-more-restrictive.md | 4.31 确保用户的主目录权限为 750 或更严格             | benchmarks/system-configurations    | 1     |
| 4.32     | 4.32-ensure-users-own-their-home-directories.md              | 4.32 确保用户拥有自己的主目录                        | benchmarks/system-configurations    | 1     |
| 4.33     | 4.33-ensure-users-dot-files-are-not-group-or-world-writable.md | 4.33 确保用户的 dot files 权限配置正确               | benchmarks/system-configurations    | 1     |
| 4.34     | 4.34-ensure-no-users-have-.forward-files.md                  | 4.34 确保没有用户拥有 .forward 文件                  | benchmarks/system-configurations    | 1     |
| 4.35     | 4.35-ensure-no-users-have-.netrc-files.md                    | 4.35 确保没有用户拥有 .netrc 文件                    | benchmarks/system-configurations    | 1     |
| 4.36     | 4.36-ensure-users-.netrc-files-are-not-group-or-world-accessible.md | 4.36 确保用户 .netrc 文件权限配置正确                | benchmarks/system-configurations    | 1     |
| 4.37     | 4.37-ensure-no-users-have-.rhosts-files.md                   | 4.37 确保没有用户拥有 .rhosts 文件                   | benchmarks/system-configurations    | 1     |
| 4.38     | 4.38-ensure-all-groups-in-/etc/passwd-exist-in-/etc/group.md | 4.38 确保 /etc/passwd 中所有组都存在于 /etc/group 中 | benchmarks/system-configurations    | 2     |
| 4.39     | 4.39-ensure-no-duplicate-uids-exist.md                       | 4.39 确保没有重复的 UID                              | benchmarks/system-configurations    | 2     |
| 4.40     | 4.40-ensure-no-duplicate-gids-exist.md                       | 4.40 确保没有重复的 GID                              | benchmarks/system-configurations    | 2     |
| 4.41     | 4.41-ensure-no-duplicate-user-names-exist.md                 | 4.41 确保没有重复的用户名                            | benchmarks/system-configurations    | 2     |
| 4.42     | 4.42-ensure-no-duplicate-group-names-exist.md                | 4.42 确保没有重复的组名                              | benchmarks/system-configurations    | 2     |
| 4.43     | 4.43-ensure-all-users-home-directories-exist.md              | 4.43 确保所有用户的主目录都存在                      | benchmarks/system-configurations    | 1     |
| 4.44     | 4.44-ensure-sctp-is-disabled.md                              | 4.44 确保禁用 SCTP                                   | benchmarks/system-configurations    | 1     |
| 4.45     | 4.45-ensure-dccp-is-disabled.md                              | 4.45 确保禁用 DCCP                                   | benchmarks/system-configurations    | 1     |
| 4.46     | 4.46-ensure-wireless-interfaces-are-disabled.md              | 4.46 确保禁用无线网卡接口                            | benchmarks/system-configurations    | 1     |
| 4.47     | 4.47-ensure-ip-forwarding-is-disabled.md                     | 4.47 确保禁用 IP 转发功能                            | benchmarks/system-configurations    | 1     |
| 4.48     | 4.48-ensure-packet-redirect-sending-is-disabled.md           | 4.48 确保禁用报文重定向发送                          | benchmarks/system-configurations    | 1     |
| 4.49     | 4.49-ensure-source-routed-packets-are-not-accepted.md        | 4.49 确保不接受源路由报文                            | benchmarks/system-configurations    | 1     |
| 4.50     | 4.50-ensure-icmp-redirects-are-not-accepted.md               | 4.50 确保不接受 ICMP 重定向                          | benchmarks/system-configurations    | 1     |
| 4.51     | 4.51-ensure-secure-icmp-redirects-are-not-accepted.md        | 4.51 确保不接受安全的ICMP重定向                      | benchmarks/system-configurations    | 1     |
| 4.52     | 4.52-ensure-suspicious-packets-are-logged.md                 | 4.52 确保对可疑报文进行日志记录                      | benchmarks/system-configurations    | 1     |
| 4.53     | 4.53-ensure-broadcast-icmp-requests-are-ignored.md           | 4.53 确保忽略 ICMP 广播请求                          | benchmarks/system-configurations    | 1     |
| 4.54     | 4.54-ensure-bogus-icmp-responses-are-ignored.md              | 4.54 确保忽略伪造的 ICMP 响应                        | benchmarks/system-configurations    | 1     |
| 4.55     | 4.55-ensure-reverse-path-filtering-is-enabled.md             | 4.55 确保启用反向路径过滤                            | benchmarks/system-configurations    | 1     |
| 4.56     | 4.56-ensure-tcp-syn-cookies-is-enabled.md                    | 4.56 确保已启用 TCP SYN cookie                       | benchmarks/system-configurations    | 1     |
| 4.57     | 4.57-ensure-ipv6-router-advertisements-are-not-accepted.md   | 4.57 确保不接受 IPv6 路由器通告                      | benchmarks/system-configurations    | 1     |
| 4.58     | 4.58-ensure-a-firewall-package-is-installed.md               | 4.58 确保已安装防火墙软件包                          | benchmarks/system-configurations    | 1     |
| 4.59     | 4.59-ensure-firewalld-service-is-enabled-and-running.md      | 4.59 确保防火墙服务已启用且运行状态正常              | benchmarks/system-configurations    | 1     |
| 4.60     | 4.60-ensure-iptables-is-not-enabled.md                       | 4.60 确保 iptables 未启用                            | benchmarks/system-configurations    | 1     |
| 4.61     | 4.61-ensure-nftables-is-not-enabled.md                       | 4.61 确保 nftables 未启用                            | benchmarks/system-configurations    | 1     |
| 4.62     | 4.62-ensure-nftables-service-is-enabled.md                   | 4.62 确保 nftables 服务已启用                        | benchmarks/system-configurations    | 1     |
| 4.63     | 4.63-ensure-iptables-packages-are-installed.md               | 4.63 确保正确安装 iptables 软件包                    | benchmarks/system-configurations    | 1     |
| 4.64     | 4.64-ensure-nftables-is-not-installed.md                     | 4.64 确保未安装 nftables                             | benchmarks/system-configurations    | 1     |
| 4.65     | 4.65-ensure-firewalld-is-not-installed-or-stopped-and-masked.md | 4.65 确保防火墙没有安装或服务已停止                  | benchmarks/system-configurations    | 1     |
| 4.66     | 4.66-ensure-system-histsize-as-100-or-other.md               | 4.66 限制历史命令记录数量                            | benchmarks/system-configurations    | 1     |
| 4.67     | 4.67-ensure-system-histfilesize-100.md                       | 4.67 限制历史命令存储文件的保存数量                  | benchmarks/system-configurations    | 1     |
| 4.68     | 4.68-ensure-permissions-TMP-is-correct.md                    | 4.68 为公共目录/tmp添加粘贴位                        | benchmarks/system-configurations    | 1     |
| 4.69     | 4.69-ensure-permissions-on-ssh-priv-and-pub-key-are-right.md | 4.69 严格要求SSH公私钥文件权限配置正确               | benchmarks/system-configurations    | 3     |
| 4.70     | 4.70-ensure-xdmcp-is-not-enabled.md                          | 4.70 确保没有启用XDMCP                               | benchmarks/system-configurations    | 1     |
| 4.71     | 4.71-ensure-nosuid-option-set-on-var-partition-Automated.md | 4.71 确保/var分区上设置nosuid选项                    | benchmarks/system-configurations    | 3     |
| 5.1      | 5.1-ensure-selinux-is-installed.md                           | 5.1 确保SElinux工具已安装                            | benchmarks/mandatory-access-control | 1     |
| 5.2      | 5.2-ensure-selinux-policy-is-configured.md                   | 5.2 确保SELinux调用mls策略                           | benchmarks/mandatory-access-control | 3     |
| 5.3      | 5.3-ensure-the-selinux-mode-is-enabled.md                    | 5.3 确保SELinux不是禁用模式                          | benchmarks/mandatory-access-control | 3     |
| 5.4      | 5.4-ensure-the-selinux-mode-is-enforcing.md                  | 5.4 确保SELinux是Enforcing模式                       | benchmarks/mandatory-access-control | 3     |
| 5.5      | 5.5-ensure-no-unconfined-services-exist.md                   | 5.5 确保没有未限制的服务存在                         | benchmarks/mandatory-access-control | 4     |
| 5.6      | 5.6-use-selinux-for-separation-of-powers-user-created.md     | 5.6 使用SELinux实现三权分离-用户创建                 | benchmarks/mandatory-access-control | 4     |
| 5.7      | 5.7-use-selinux-for-separation-of-powers-system-administrator-login-permission-configuration.md | 5.7 使用SELinux实现三权分离-系统管理员登录权限配置   | benchmarks/mandatory-access-control | 4     |
| 5.8      | 5.8-create-common-audit-and-security-users.md                | 5.8 创建普通、审计、安全用户                         | benchmarks/mandatory-access-control | 2     |
| 5.9      | 5.9-ensure-setroubleshoot-is-not-installed.md                | 5.9 确保setroubleshoot被卸载                        | benchmarks/mandatory-access-control | 1     |